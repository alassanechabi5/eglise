<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_galleries', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('image');
            $table->bigInteger('history_id')->unsigned();
            $table->foreign('history_id')->references('id')->on('histories')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_galleries');
    }
}
