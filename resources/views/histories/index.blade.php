@extends('layouts.app')

@section('title') {{\Auth::user()->branchname}}{{\Auth::user()->branchcode}}: Histoires Report @endsection

@section('link')
<link rel="stylesheet" href="{{URL::asset('css/pignose.calendar.min.css')}}">
@endsection
@section('content')
<?php

// extract addedVariables value to variable
if (isset($addedVariables)) extract($addedVariables);

?>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<style>
li {
    display: inline;
}
/* Dropdown Button */
.dropbtn {
    background-color: #4CAF50;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
    position: relative;
    display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #ddd;}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #3e8e41;}

.icon {
  font-size: 100px;
}
</style>
<div id="content-container">
    <div id="page-head">

        <!--Page Title-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div id="page-title">
            <h1 class="page-header text-overflow">Histoire</h1>
        </div>
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <!--End page title-->


        <!--Breadcrumb-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <ol class="breadcrumb">
          <li>
              <i class="fa fa-home"></i><a href="{{route('dashboard')}}"> Dashboard</a>
          </li>
            <li class="active">All</li>
        </ol>
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <!--End breadcrumb-->

    </div>


    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">
        <div class="row">
            <div class="col-md-6 col-md-offset-3"  >
                @if (session('status'))

                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)

                        <div class="alert alert-danger">{{ $error }}</div>

                    @endforeach

                @endif
            </div>
            
            <div class="col-md-offset-1 col-md-10" style="margin-bottom:50px">
              <div class="panel rounded-top" style="background-color: #e8ddd3;">
                <div class="panel-heading text-center">
                  <h1 class="panel-title">Branch history History<h1>
                </div>
                <div class="panel-body clearfix table-resposive">
                  <table id="demo-dt-basic" class="table table-striped table-bordered datatable" cellspacing="0" width="100%" >
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th class="min-tablet">Title</th>
                        <th class="min-tablet">Début</th>
                        <th class="min-tablet">Fin</th>
                        <th class="min-tablet">Histoire</th> 
                        <th class="min-desktop">Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php $count=1;?>
                    @foreach($histoires as $list)
                    <?php
                      $date = $list->date_debut;
                      $d = date("F,Y,D", strtotime($date));
                      $p = explode(',',$d);
                    ?>
                    <tr>
                        <td><strong>{{$count}}</strong></td>
                        <td>{{$list->title}}</td>
                        <td>{{$list->date_debut}}</td>
                        <td>{{$list->date_debut}}</td>
                        <td>{{$list->comment}}</td>
                        <td><button id="{{$list->date_debut}}" type="submit" class="btn btn-primary viewBtn" onclick="viewer(this);">View</button></td>
                    </tr>
                    <?php $count++;?>
                    @endforeach
                </tbody>
            </table>
          </div>
          </div>
        </div>
 

          <!-- ATTENDANCE Modal -->
          <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width: 50%; margin: 0 auto;">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header bg-warning">
                  <button type="button" class="close" data-dismiss="modal"><h1>&times;</h1></button>
                  <div class="d-inline pull-left"><h1 class="">Date: </h1></div>
                  <div class="d-inline text-center text-white"><h1 id="date-title"></h1></div>
                </div>
                <div id="modal-body" class="modal-body">

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

          <!-- MEMBER ATTENDANCE Modal -->
          <div id="memberAttendanceModal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width: 50%; margin: 0 auto;">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header bg-warning">
                  <button type="button" class="close" data-dismiss="modal"><h1>&times;</h1></button>
                  <!-- <div class="d-inline pull-left"><h1 class="">Date: </h1></div> -->
                  <div class="d-inline text-center text-white"><h1 id="member-fullname"></h1></div>
                </div>
                <div id="modal-body" class="modal-body">
                  <div class="table-responsive">
                    <table id="member-attendance-table" class="table table-striped table-bordered" cellspacing="0" width="100%" >
                      <thead></thead>
                      <tbody> </tbody>
                    </table>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

        </div>
    </div>
    <!--===================================================-->
    <!--End page content-->
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
@endsection

@section('js')
<script src="{{ URL::asset('js/functions.js') }}"></script>
<script src="{{URL::asset('js/pignose.calendar.full.min.js')}}"></script>
<script>
$(document).ready(() => {
  $(function() {
    $('.widget-calender').pignoseCalendar({
      theme: 'blue',
      select: handleSelect,
    });
  });
  //Attnedance Module
  $('#view-year').click(function (){
  	$('#show-year').show();
    // loadElement($('#viewByDate').find(':submit'), true, 'fetching')
  });

  $('#viewByDate').submit((e) => {
    e.preventDefault()
    submit = $('#viewByDate').find(':submit')
    toggleAble(submit, true, 'fetching')
    let date = $('#yearDate').val()
    let h1 = document.createElement('h1')
    $(h1).attr('id')
    h1.setAttribute("id", date);
  	view(h1, () => {
      toggleAble(submit, false)
    })
  });

  // member attendance event
  $('.show-member-history').click( e => {
    const id = $(e.target).data('id')
    const fullname = $(e.target).data('fullname')
    $('#member-fullname').text(fullname)
    viewMemberAttendance(id, $(e.target))
  })

})
const viewer = (element) => {
  loadElement($(element), true)
  view(element, () => {
    loadElement($(element), false)
  })
}

function handleSelect(date, context){
  let h1 = document.createElement('h1')
  $(h1).attr('id')
  let sdate = date[0].format('YYYY-MM-DD');
  h1.setAttribute("id", sdate);
  view(h1, () => {})
}

function showe(date){
  $('#date-title').html(date)
  $('#myModal').modal('show')
}

  
</script>
@endsection
