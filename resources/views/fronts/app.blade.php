<!doctype html>
<html xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml" lang="en-US">

<!-- Mirrored from www.crosspointwaverly.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 23 Dec 2022 08:51:50 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->

<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- This is Squarespace. --><!-- glockenspiel-marigold-78yl -->
  <base #href="">
  <meta charset="utf-8" />
  <title>Crosspoint Church</title>
  <link rel="shortcut icon" type="image/x-icon"
    href="https://images.squarespace-cdn.com/content/v1/6298e9ea65c3d6676b87e494/4256749c-c8f8-4f62-909a-ac909a6b7bd7/favicon.ico?format=100w')}}" />
  <link rel="canonical" href="index.html" />
  <meta property="og:site_name" content="Crosspoint Church" />
  <meta property="og:title" content="Crosspoint Church" />
  <meta property="og:url" content="index.html" />
  <meta property="og:type" content="website" />
  <meta property="og:image"
    content="{{ URL::asset('imageschurch/content/6298e9ea65c3d6676b87e494/bb7d914c-e9f3-4bbc-a70e-f7ba89580b9b/cpc%2blogo43a3.jpg?format=1500w')}}" />
  <meta property="og:image:width" content="1500" />
  <meta property="og:image:height" content="1274" />
  <meta itemprop="name" content="Crosspoint Church" />
  <meta itemprop="url" content="index.html" />
  <meta itemprop="thumbnailUrl"
    content="{{ URL::asset('imageschurch/content/6298e9ea65c3d6676b87e494/bb7d914c-e9f3-4bbc-a70e-f7ba89580b9b/cpc%2blogo43a3.jpg?format=1500w')}}" />
  <link rel="image_src"
    href="{{ URL::asset('imageschurch/content/6298e9ea65c3d6676b87e494/bb7d914c-e9f3-4bbc-a70e-f7ba89580b9b/cpc%2blogo43a3.jpg?format=1500w')}}" />
  <meta itemprop="image"
    content="{{ URL::asset('imageschurch/content/6298e9ea65c3d6676b87e494/bb7d914c-e9f3-4bbc-a70e-f7ba89580b9b/cpc%2blogo43a3.jpg?format=1500w')}}" />
  <meta name="twitter:title" content="Crosspoint Church" />
  <meta name="twitter:image"
    content="{{ URL::asset('imageschurch/content/6298e9ea65c3d6676b87e494/bb7d914c-e9f3-4bbc-a70e-f7ba89580b9b/cpc%2blogo43a3.jpg?format=1500w')}}" />
  <meta name="twitter:url" content="index.html" />
  <meta name="twitter:card" content="summary" />
  <meta name="description" content="" />
  <link rel="preconnect" href="https://images.squarespace-cdn.com/">
  <link rel="stylesheet" type="text/css"
    href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,700;1,400&amp;family=Poppins:wght@500">
  <script type="text/javascript" crossorigin="anonymous" defer="defer" nomodule="nomodule"
    src="{{ URL::asset('assetschurch/%40sqs/polyfiller/1.6/legacy.js')}}"></script>
  <script type="text/javascript" crossorigin="anonymous" defer="defer"
    src="{{ URL::asset('assetschurch/%40sqs/polyfiller/1.6/modern.js')}}"></script>
  <script type="text/javascript">SQUARESPACE_ROLLUPS = {};</script>
  <script>(function (rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/extract-css-runtime-02c487476f1fda28389eb-min.en-US.js')}}"]; })(SQUARESPACE_ROLLUPS, 'squarespace-extract_css_runtime');</script>
  <script crossorigin="anonymous"
    src="{{ URL::asset('assetschurch/universal/scripts-compressed/extract-css-runtime-02c487476f1fda28389eb-min.en-US.js')}}"
    defer></script>
  <script>(function (rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/extract-css-moment-js-vendor-5082e2dab696b020ac83a-min.en-US.js')}}"]; })(SQUARESPACE_ROLLUPS, 'squarespace-extract_css_moment_js_vendor');</script>
  <script crossorigin="anonymous"
    src="{{ URL::asset('assetschurch/universal/scripts-compressed/extract-css-moment-js-vendor-5082e2dab696b020ac83a-min.en-US.js')}}"
    defer></script>
  <script>(function (rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/cldr-resource-pack-d7070e8b408cf38ac16f3-min.en-US.js')}}"]; })(SQUARESPACE_ROLLUPS, 'squarespace-cldr_resource_pack');</script>
  <script crossorigin="anonymous"
    src="{{ URL::asset('assetschurch/universal/scripts-compressed/cldr-resource-pack-d7070e8b408cf38ac16f3-min.en-US.js')}}"
    defer></script>
  <script>(function (rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-vendors-stable-673d482d883e1c7ecd140-min.en-US.js')}}"]; })(SQUARESPACE_ROLLUPS, 'squarespace-common_vendors_stable');</script>
  <script crossorigin="anonymous"
    src="{{ URL::asset('assetschurch/universal/scripts-compressed/common-vendors-stable-673d482d883e1c7ecd140-min.en-US.js')}}"
    defer></script>
  <script>(function (rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-vendors-67169371a6a1d02579cc5-min.en-US.js')}}"]; })(SQUARESPACE_ROLLUPS, 'squarespace-common_vendors');</script>
  <script crossorigin="anonymous"
    src="{{ URL::asset('assetschurch/universal/scripts-compressed/common-vendors-67169371a6a1d02579cc5-min.en-US.js')}}"
    defer></script>
  <script>(function (rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-1be3fc0a28db4edf443fa-min.en-US.js')}}"]; })(SQUARESPACE_ROLLUPS, 'squarespace-common');</script>
  <script crossorigin="anonymous"
    src="{{ URL::asset('assetschurch/universal/scripts-compressed/common-1be3fc0a28db4edf443fa-min.en-US.js')}}"
    defer></script>
  <script>(function (rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/commerce-c3e96983d17424d479b0e-min.en-US.js')}}"]; })(SQUARESPACE_ROLLUPS, 'squarespace-commerce');</script>
  <script crossorigin="anonymous"
    src="{{ URL::asset('assetschurch/universal/scripts-compressed/commerce-c3e96983d17424d479b0e-min.en-US.js')}}"
    defer></script>
  <script>(function (rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].css = ["//assets.squarespace.com/universal/styles-compressed/commerce-42e904b2189a7c1684dd6-min.en-US.css"]; })(SQUARESPACE_ROLLUPS, 'squarespace-commerce');</script>
  <link rel="stylesheet" type="text/css"
    href="{{ URL::asset('assetschurch/universal/styles-compressed/commerce-42e904b2189a7c1684dd6-min.en-US.css">
  <script>(function (rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/performance-ab3c5a52dcba10089a517-min.en-US.js')}}"]; })(SQUARESPACE_ROLLUPS, 'squarespace-performance');</script>
  <script crossorigin="anonymous"
    src="{{ URL::asset('assetschurch/universal/scripts-compressed/performance-ab3c5a52dcba10089a517-min.en-US.js')}}"
    defer></script>
  <script
    data-name="static-context">Static = window.Static || {}; Static.SQUARESPACE_CONTEXT = { "facebookAppId": "314192535267336", "facebookApiVersion": "v6.0", "rollups": { "squarespace-announcement-bar": { "js')}}": "//assets.squarespace.com/universal/scripts-compressed/announcement-bar-d477516abab83e8704450-min.en-US.js')}}" }, "squarespace-audio-player": { "css": "//assets.squarespace.com/universal/styles-compressed/audio-player-702bf18174efe0acaa8ce-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/audio-player-68a06f16b643aa2c1696b-min.en-US.js')}}" }, "squarespace-blog-collection-list": { "css": "//assets.squarespace.com/universal/styles-compressed/blog-collection-list-3d55c64c25996c7633fc2-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/blog-collection-list-a9d6e2a5fc511c5f6dd74-min.en-US.js')}}" }, "squarespace-calendar-block-renderer": { "css": "//assets.squarespace.com/universal/styles-compressed/calendar-block-renderer-49c4a5f3dae67a728e3f4-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/calendar-block-renderer-8e3e0734c85178000cf34-min.en-US.js')}}" }, "squarespace-chartjs-helpers": { "css": "//assets.squarespace.com/universal/styles-compressed/chartjs-helpers-53c004ac7d4bde1c92e38-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/chartjs-helpers-fcdcaf3bdda14c3ad30df-min.en-US.js')}}" }, "squarespace-comments": { "css": "//assets.squarespace.com/universal/styles-compressed/comments-91e23fb14e407e3111565-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/comments-a66fe4d73d819246c8de4-min.en-US.js')}}" }, "squarespace-dialog": { "css": "//assets.squarespace.com/universal/styles-compressed/dialog-89b254b5c87045b9e1360-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/dialog-97c4f5af39712db17be01-min.en-US.js')}}" }, "squarespace-events-collection": { "css": "//assets.squarespace.com/universal/styles-compressed/events-collection-49c4a5f3dae67a728e3f4-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/events-collection-8c3d66ee8b3e79f91ded0-min.en-US.js')}}" }, "squarespace-form-rendering-utils": { "js')}}": "//assets.squarespace.com/universal/scripts-compressed/form-rendering-utils-beb39e7070c5b3fae04e3-min.en-US.js')}}" }, "squarespace-forms": { "css": "//assets.squarespace.com/universal/styles-compressed/forms-4a16a8a8c965386db2173-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/forms-c1d254ec34e92cfbba88d-min.en-US.js')}}" }, "squarespace-gallery-collection-list": { "css": "//assets.squarespace.com/universal/styles-compressed/gallery-collection-list-3d55c64c25996c7633fc2-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/gallery-collection-list-df911470e9b09f49f1e70-min.en-US.js')}}" }, "squarespace-image-zoom": { "css": "//assets.squarespace.com/universal/styles-compressed/image-zoom-3d55c64c25996c7633fc2-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/image-zoom-0eab58eaec9ee30292230-min.en-US.js')}}" }, "squarespace-pinterest": { "css": "//assets.squarespace.com/universal/styles-compressed/pinterest-3d55c64c25996c7633fc2-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/pinterest-3b4e948d4c474baa2e0ef-min.en-US.js')}}" }, "squarespace-popup-overlay": { "css": "//assets.squarespace.com/universal/styles-compressed/popup-overlay-948192219c3257f767ec5-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/popup-overlay-da6410296f9bb8ee0b0d0-min.en-US.js')}}" }, "squarespace-product-quick-view": { "css": "//assets.squarespace.com/universal/styles-compressed/product-quick-view-4a16a8a8c965386db2173-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/product-quick-view-c930ac516cc60f42d8618-min.en-US.js')}}" }, "squarespace-products-collection-item-v2": { "css": "//assets.squarespace.com/universal/styles-compressed/products-collection-item-v2-3d55c64c25996c7633fc2-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/products-collection-item-v2-8c110f8a9588de4ad24eb-min.en-US.js')}}" }, "squarespace-products-collection-list-v2": { "css": "//assets.squarespace.com/universal/styles-compressed/products-collection-list-v2-3d55c64c25996c7633fc2-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/products-collection-list-v2-a9c6ec1696c82409bd013-min.en-US.js')}}" }, "squarespace-search-page": { "css": "//assets.squarespace.com/universal/styles-compressed/search-page-9d0a55de1efafbb9218e1-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/search-page-08f7e8d6217925f6b8874-min.en-US.js')}}" }, "squarespace-search-preview": { "js')}}": "//assets.squarespace.com/universal/scripts-compressed/search-preview-2e1ab29441dc195a8a6fc-min.en-US.js')}}" }, "squarespace-simple-liking": { "css": "//assets.squarespace.com/universal/styles-compressed/simple-liking-ef94529873378652e6e86-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/simple-liking-000ca46b0287522a4ecd4-min.en-US.js')}}" }, "squarespace-social-buttons": { "css": "//assets.squarespace.com/universal/styles-compressed/social-buttons-1f18e025ea682ade6293a-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/social-buttons-4181ec45180418a082544-min.en-US.js')}}" }, "squarespace-tourdates": { "css": "//assets.squarespace.com/universal/styles-compressed/tourdates-3d55c64c25996c7633fc2-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/tourdates-3fc4e93c9d4b655cf609d-min.en-US.js')}}" }, "squarespace-website-overlays-manager": { "css": "//assets.squarespace.com/universal/styles-compressed/website-overlays-manager-7cecc648f858e6f692130-min.en-US.css", "js')}}": "//assets.squarespace.com/universal/scripts-compressed/website-overlays-manager-6ffad8883d36b46332c60-min.en-US.js')}}" } }, "pageType": 2, "website": { "id": "6298e9ea65c3d6676b87e494", "identifier": "glockenspiel-marigold-78yl", "websiteType": 4, "contentModifiedOn": 1666888015030, "cloneable": false, "hasBeenCloneable": false, "siteStatus": {}, "language": "en-US", "timeZone": "America/Chicago", "machineTimeZoneOffset": -21600000, "timeZoneOffset": -21600000, "timeZoneAbbr": "CST", "siteTitle": "Crosspoint Church", "fullSiteTitle": "Crosspoint Church", "siteDescription": "", "logoImageId": "62aa4d1697c42950b15f2910", "shareButtonOptions": { "8": true, "1": true, "2": true, "4": true, "3": true, "6": true, "7": true }, "logoImageUrl": "//images.squarespace-cdn.com/content/v1/6298e9ea65c3d6676b87e494/bb7d914c-e9f3-4bbc-a70e-f7ba89580b9b/cpc+logo.jpg", "authenticUrl": "https://www.crosspointwaverly.com", "internalUrl": "https://glockenspiel-marigold-78yl.squarespace.com", "baseUrl": "https://www.crosspointwaverly.com", "primaryDomain": "www.crosspointwaverly.com", "sslSetting": 3, "isHstsEnabled": true, "socialAccounts": [{ "serviceId": 60, "screenname": "Facebook", "addedOn": 1655394379684, "profileUrl": "https://www.facebook.com/cpcwaverly/", "iconEnabled": true, "serviceName": "facebook-unauth" }, { "serviceId": 69, "screenname": "YouTube", "addedOn": 1660079323690, "profileUrl": "https://www.youtube.com/channel/UCpFsNYvlHPRlS_-96JdbBZQ", "iconEnabled": true, "serviceName": "youtube-unauth" }, { "serviceId": 64, "screenname": "Instagram", "addedOn": 1655394539673, "profileUrl": "https://www.instagram.com/cpcwaverly/", "iconEnabled": true, "serviceName": "instagram-unauth" }, { "serviceId": 71, "screenname": "Spotify", "addedOn": 1660079299142, "profileUrl": "https://open.spotify.com/show/47YHBzNf52F6IhX9wJQESi", "iconEnabled": true, "serviceName": "spotify-unauth" }], "typekitId": "", "statsMigrated": false, "imageMetadataProcessingEnabled": false, "captchaSettings": { "enabledForDonations": false }, "showOwnerLogin": false }, "websiteSettings": { "id": "6298e9eb65c3d6676b87e496", "websiteId": "6298e9ea65c3d6676b87e494", "subjects": [], "country": "US", "state": "IA", "simpleLikingEnabled": true, "mobileInfoBarSettings": { "isContactEmailEnabled": false, "isContactPhoneNumberEnabled": false, "isLocationEnabled": false, "isBusinessHoursEnabled": false }, "commentLikesAllowed": true, "commentAnonAllowed": true, "commentThreaded": true, "commentApprovalRequired": false, "commentAvatarsOn": true, "commentSortType": 2, "commentFlagThreshold": 0, "commentFlagsAllowed": true, "commentEnableByDefault": true, "commentDisableAfterDaysDefault": 0, "disqusShortname": "", "commentsEnabled": false, "storeSettings": { "returnPolicy": null, "termsOfService": null, "privacyPolicy": null, "expressCheckout": false, "continueShoppingLinkUrl": "/", "useLightCart": false, "showNoteField": false, "shippingCountryDefaultValue": "US", "billToShippingDefaultValue": false, "showShippingPhoneNumber": true, "isShippingPhoneRequired": false, "showBillingPhoneNumber": true, "isBillingPhoneRequired": false, "currenciesSupported": ["USD", "CAD", "GBP", "AUD", "EUR", "CHF", "NOK", "SEK", "DKK", "NZD", "SGD", "MXN", "HKD", "CZK", "ILS", "MYR", "RUB", "PHP", "PLN", "THB", "BRL", "ARS", "COP", "IDR", "INR", "JPY", "ZAR"], "defaultCurrency": "USD", "selectedCurrency": "USD", "measurementStandard": 1, "showCustomCheckoutForm": false, "checkoutPageMarketingOptInEnabled": true, "enableMailingListOptInByDefault": false, "sameAsRetailLocation": false, "merchandisingSettings": { "scarcityEnabledOnProductItems": false, "scarcityEnabledOnProductBlocks": false, "scarcityMessageType": "DEFAULT_SCARCITY_MESSAGE", "scarcityThreshold": 10, "multipleQuantityAllowedForServices": true, "restockNotificationsEnabled": false, "restockNotificationsMailingListSignUpEnabled": false, "relatedProductsEnabled": false, "relatedProductsOrdering": "random", "soldOutVariantsDropdownDisabled": false, "productComposerOptedIn": false, "productComposerABTestOptedOut": false, "productReviewsEnabled": false }, "isLive": false, "multipleQuantityAllowedForServices": true }, "useEscapeKeyToLogin": false, "ssBadgeType": 1, "ssBadgePosition": 4, "ssBadgeVisibility": 1, "ssBadgeDevices": 1, "pinterestOverlayOptions": { "mode": "disabled" }, "ampEnabled": false }, "cookieSettings": { "isCookieBannerEnabled": false, "isRestrictiveCookiePolicyEnabled": false, "isRestrictiveCookiePolicyAbsolute": false, "cookieBannerText": "", "cookieBannerTheme": "", "cookieBannerVariant": "", "cookieBannerPosition": "", "cookieBannerCtaVariant": "", "cookieBannerCtaText": "", "cookieBannerAcceptType": "OPT_IN", "cookieBannerOptOutCtaText": "" }, "websiteCloneable": false, "collection": { "title": "Home", "id": "6298ea29a2382b218be83236", "fullUrl": "/", "type": 10, "permissionType": 1 }, "subscribed": false, "appDomain": "squarespace.com", "templateTweakable": true, "tweakJSON": { "header-logo-height": "56px", "header-mobile-logo-max-height": "52px", "header-vert-padding": "2vw", "header-width": "Full", "maxPageWidth": "1200px", "pagePadding": "3vw", "tweak-blog-alternating-side-by-side-image-aspect-ratio": "1:1 Square", "tweak-blog-alternating-side-by-side-image-spacing": "5%", "tweak-blog-alternating-side-by-side-meta-spacing": "15px", "tweak-blog-alternating-side-by-side-primary-meta": "Categories", "tweak-blog-alternating-side-by-side-read-more-spacing": "10px", "tweak-blog-alternating-side-by-side-secondary-meta": "Date", "tweak-blog-basic-grid-columns": "2", "tweak-blog-basic-grid-image-aspect-ratio": "16:9 Widescreen", "tweak-blog-basic-grid-image-spacing": "30px", "tweak-blog-basic-grid-meta-spacing": "15px", "tweak-blog-basic-grid-primary-meta": "Categories", "tweak-blog-basic-grid-read-more-spacing": "15px", "tweak-blog-basic-grid-secondary-meta": "Date", "tweak-blog-item-custom-width": "60", "tweak-blog-item-show-author-profile": "true", "tweak-blog-item-width": "Narrow", "tweak-blog-masonry-columns": "2", "tweak-blog-masonry-horizontal-spacing": "150px", "tweak-blog-masonry-image-spacing": "25px", "tweak-blog-masonry-meta-spacing": "20px", "tweak-blog-masonry-primary-meta": "Categories", "tweak-blog-masonry-read-more-spacing": "5px", "tweak-blog-masonry-secondary-meta": "Date", "tweak-blog-masonry-vertical-spacing": "100px", "tweak-blog-side-by-side-image-aspect-ratio": "1:1 Square", "tweak-blog-side-by-side-image-spacing": "6%", "tweak-blog-side-by-side-meta-spacing": "20px", "tweak-blog-side-by-side-primary-meta": "Categories", "tweak-blog-side-by-side-read-more-spacing": "5px", "tweak-blog-side-by-side-secondary-meta": "Date", "tweak-blog-single-column-image-spacing": "40px", "tweak-blog-single-column-meta-spacing": "30px", "tweak-blog-single-column-primary-meta": "Categories", "tweak-blog-single-column-read-more-spacing": "5px", "tweak-blog-single-column-secondary-meta": "Date", "tweak-events-stacked-show-thumbnails": "true", "tweak-events-stacked-thumbnail-size": "3:2 Standard", "tweak-fixed-header": "false", "tweak-fixed-header-style": "Scroll Back", "tweak-global-animations-animation-curve": "ease", "tweak-global-animations-animation-delay": "0.1s", "tweak-global-animations-animation-duration": "0.1s", "tweak-global-animations-animation-style": "fade", "tweak-global-animations-animation-type": "none", "tweak-global-animations-complexity-level": "detailed", "tweak-global-animations-enabled": "false", "tweak-portfolio-grid-basic-custom-height": "50", "tweak-portfolio-grid-overlay-custom-height": "50", "tweak-portfolio-hover-follow-acceleration": "10%", "tweak-portfolio-hover-follow-animation-duration": "Fast", "tweak-portfolio-hover-follow-animation-type": "Fade", "tweak-portfolio-hover-follow-delimiter": "Bullet", "tweak-portfolio-hover-follow-front": "false", "tweak-portfolio-hover-follow-layout": "Inline", "tweak-portfolio-hover-follow-size": "50", "tweak-portfolio-hover-follow-text-spacing-x": "1.5", "tweak-portfolio-hover-follow-text-spacing-y": "1.5", "tweak-portfolio-hover-static-animation-duration": "Fast", "tweak-portfolio-hover-static-animation-type": "Fade", "tweak-portfolio-hover-static-delimiter": "Hyphen", "tweak-portfolio-hover-static-front": "true", "tweak-portfolio-hover-static-layout": "Inline", "tweak-portfolio-hover-static-size": "50", "tweak-portfolio-hover-static-text-spacing-x": "1.5", "tweak-portfolio-hover-static-text-spacing-y": "1.5", "tweak-portfolio-index-background-animation-duration": "Medium", "tweak-portfolio-index-background-animation-type": "Fade", "tweak-portfolio-index-background-custom-height": "50", "tweak-portfolio-index-background-delimiter": "None", "tweak-portfolio-index-background-height": "Large", "tweak-portfolio-index-background-horizontal-alignment": "Center", "tweak-portfolio-index-background-link-format": "Stacked", "tweak-portfolio-index-background-persist": "false", "tweak-portfolio-index-background-vertical-alignment": "Middle", "tweak-portfolio-index-background-width": "Full Bleed", "tweak-product-basic-item-click-action": "None", "tweak-product-basic-item-gallery-aspect-ratio": "3:4 Three-Four (Vertical)", "tweak-product-basic-item-gallery-design": "Slideshow", "tweak-product-basic-item-gallery-width": "50%", "tweak-product-basic-item-hover-action": "None", "tweak-product-basic-item-image-spacing": "3vw", "tweak-product-basic-item-image-zoom-factor": "1.75", "tweak-product-basic-item-thumbnail-placement": "Side", "tweak-product-basic-item-variant-picker-layout": "Dropdowns", "tweak-products-columns": "3", "tweak-products-gutter-column": "2vw", "tweak-products-gutter-row": "3vw", "tweak-products-header-text-alignment": "Middle", "tweak-products-image-aspect-ratio": "1:1 Square", "tweak-products-image-text-spacing": "1vw", "tweak-products-text-alignment": "Left", "tweak-transparent-header": "false" }, "templateId": "5c5a519771c10ba3470d8101", "templateVersion": "7.1", "pageFeatures": [1, 2, 4], "gmRenderKey": "QUl6YVN5Q0JUUk9xNkx1dkZfSUUxcjQ2LVQ0QWVUU1YtMGQ3bXk4", "templateScriptsRootUrl": "https://static1.squarespace.com/static/vta/5c5a519771c10ba3470d8101/scripts/", "betaFeatureFlags": ["fluid_engine_clean_up_grid_contextual_change", "campaigns_thumbnail_layout", "supports_versioned_template_assets", "site_header_footer", "fluid_engine", "block_annotations_revamp", "crm_default_newsletter_block_to_campaigns", "customer_account_creation_emails", "scheduling_block_schema_editor", "background_art_onboarding", "campaigns_global_uc_ab", "crm_remove_subscriber", "crm_retention_segment", "asset_uploader_refactor", "commerce_site_visitor_metrics}}", "new_stacked_index", "member_areas_schedule_interview", "customer_accounts_email_verification", "campaigns_discount_section_in_blasts", "themes", "viewer-role-contributor-invites", "campaigns_new_image_layout_picker", "nested_categories", "commerce_etsy_product_import", "member_areas_spanish_interviews", "campaigns_content_editing_survey", "commerce_restock_notifications", "commerce_etsy_shipping_import", "send_local_pickup_ready_email", "commerce_clearpay", "campaigns_asset_picker", "marketing_landing_page", "campaigns_seasonal_inventory_page_banner", "commerce_order_status_access", "collection_typename_switching", "fluid_engine_filtered_catalog_endpoints", "campaigns_new_subscriber_search", "scripts_defer", "nested_categories_migration_enabled", "override_block_styles", "customer_account_creation_recaptcha", "campaigns_show_featured_templates"], "videoAssetsFeatureFlags": ["hls-playback", "mux-data-video-collection", "video-external-link-youtube-custom-embed-api"], "impersonatedSession": false, "demoCollections": [{ "collectionId": "5d3b3ee908aec50001a75ca7", "deleted": true }, { "collectionId": "5d40759b9a83930001d9c90b", "deleted": true }, { "collectionId": "5d4075aee783490001d530ed", "deleted": true }, { "collectionId": "5d4076809ee54a000146725c", "deleted": true }, { "collectionId": "5d4082679fa9f000016867d5", "deleted": true }, { "collectionId": "5d4083bd2d13c40001b4b81d", "deleted": true }, { "collectionId": "5d4084838b769d000167c35d", "deleted": true }, { "collectionId": "5d4086b22d13c40001b4defc", "deleted": true }, { "collectionId": "5d408751bfbcaf0001c702d6", "deleted": true }], "tzData": { "zones": [[-360, "US", "C%sT", null]], "rules": { "US": [[1967, 2006, null, "Oct", "lastSun", "2:00", "0", "S"], [1987, 2006, null, "Apr", "Sun>=1", "2:00", "1:00", "D"], [2007, "max", null, "Mar", "Sun>=8", "2:00", "1:00", "D"], [2007, "max", null, "Nov", "Sun>=1", "2:00", "0", "S"]] } }, "showAnnouncementBar": false, "recaptchaEnterpriseContext": { "recaptchaEnterpriseSiteKey": "6LdDFQwjAAAAAPigEvvPgEVbb7QBm-TkVJdDTlAv" } };</script>
  <script
    type="application/ld+json">{"url":"https://www.crosspointwaverly.com","name":"Crosspoint Church","description":"","image":"//images.squarespace-cdn.com/content/v1/6298e9ea65c3d6676b87e494/bb7d914c-e9f3-4bbc-a70e-f7ba89580b9b/cpc+logo.jpg","@context":"http://schema.org","@type":"WebSite"}</script>
  <link rel="stylesheet" type="text/css"
    href="{{ URL::asset('static1church/static/versioned-site-css/6298e9ea65c3d6676b87e494/19/5c5a519771c10ba3470d8101/6298e9ec65c3d6676b87e4e4/1328/site.css')}}" />
  <script>Static.COOKIE_BANNER_CAPABLE = true;</script>
  <!-- End of Squarespace Headers -->

  <script>
    window.__INITIAL_SQUARESPACE_7_1_WEBSITE_COLORS__ = [{ "id": "white", "value": { "values": { "hue": 0.0, "saturation": 0.0, "lightness": 100.0 }, "userFormat": "hsl" } }, { "id": "black", "value": { "values": { "hue": 0.0, "saturation": 0.0, "lightness": 0.0 }, "userFormat": "hsl" } }, { "id": "safeLightAccent", "value": { "values": { "hue": 202.40963855421685, "saturation": 35.622317596566525, "lightness": 45.68627450980392 }, "userFormat": "hex" } }, { "id": "safeDarkAccent", "value": { "values": { "hue": 202.40963855421685, "saturation": 35.622317596566525, "lightness": 45.68627450980392 }, "userFormat": "hex" } }, { "id": "safeInverseAccent", "value": { "values": { "hue": 0.0, "saturation": 0.0, "lightness": 100.0 }, "userFormat": "hex" } }, { "id": "safeInverseLightAccent", "value": { "values": { "hue": 0.0, "saturation": 0.0, "lightness": 100.0 }, "userFormat": "hex" } }, { "id": "safeInverseDarkAccent", "value": { "values": { "hue": 0.0, "saturation": 0.0, "lightness": 100.0 }, "userFormat": "hex" } }, { "id": "accent", "value": { "values": { "hue": 202.40963855421685, "saturation": 35.622317596566525, "lightness": 45.68627450980392 }, "userFormat": "hex" } }, { "id": "lightAccent", "value": { "values": { "hue": 0.0, "saturation": 0.0, "lightness": 96.86274509803921 }, "userFormat": "hsl" } }, { "id": "darkAccent", "value": { "values": { "hue": 240.0, "saturation": 2.608695652173916, "lightness": 22.54901960784314 }, "userFormat": "hex" } }];
  </script>

  <style id="colorThemeStyles">
    :root {
      --white-hsl: 0, 0%, 100%;
      --black-hsl: 0, 0%, 0%;
      --safeLightAccent-hsl: 202.40963855421685, 35.622317596566525%, 45.68627450980392%;
      --safeDarkAccent-hsl: 202.40963855421685, 35.622317596566525%, 45.68627450980392%;
      --safeInverseAccent-hsl: 0, 0%, 100%;
      --safeInverseLightAccent-hsl: 0, 0%, 100%;
      --safeInverseDarkAccent-hsl: 0, 0%, 100%;
      --accent-hsl: 202.40963855421685, 35.622317596566525%, 45.68627450980392%;
      --lightAccent-hsl: 0, 0%, 96.86274509803921%;
      --darkAccent-hsl: 240, 2.608695652173916%, 22.54901960784314%;
    }

    :root {
      --announcement-bar-background-color: hsla(var(--black-hsl), 1);
      --announcement-bar-text-color: hsla(var(--white-hsl), 1);
      --backgroundOverlayColor: hsla(var(--white-hsl), 1);
      --course-item-nav-active-lesson-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --course-item-nav-active-lesson-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --course-item-nav-background-color: hsla(var(--lightAccent-hsl), 1);
      --course-item-nav-border-color: hsla(var(--safeInverseLightAccent-hsl), 0.25);
      --course-item-nav-open-tab-background-color: hsla(var(--black-hsl), 0.75);
      --course-item-nav-open-tab-text-color: hsla(var(--white-hsl), 1);
      --course-item-nav-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --course-list-course-item-background: hsla(var(--lightAccent-hsl), 1);
      --course-list-course-item-hover-background: hsla(var(--lightAccent-hsl), 0.75);
      --course-list-course-item-text-color: hsla(var(--black-hsl), 1);
      --course-list-course-progress-bar-color: hsla(var(--black-hsl), 1);
      --gradientHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --gradientHeaderBorderColor: hsla(var(--black-hsl), 1);
      --gradientHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --gradientHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --headingExtraLargeColor: hsla(var(--black-hsl), 1);
      --headingLargeColor: hsla(var(--black-hsl), 1);
      --headingLinkColor: hsla(var(--safeDarkAccent-hsl), 1);
      --headingMediumColor: hsla(var(--black-hsl), 1);
      --headingSmallColor: hsla(var(--black-hsl), 1);
      --image-block-card-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-card-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-card-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-card-image-subtitle-color: hsla(var(--black-hsl), 1);
      --image-block-card-image-title-bg-color: hsla(var(--white-hsl), 0);
      --image-block-card-image-title-color: hsla(var(--black-hsl), 1);
      --image-block-card-inline-link-color: hsla(var(--black-hsl), 1);
      --image-block-collage-background-color: hsla(var(--lightAccent-hsl), 1);
      --image-block-collage-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-collage-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-collage-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-collage-image-subtitle-color: hsla(var(--black-hsl), 1);
      --image-block-collage-image-title-bg-color: hsla(var(--white-hsl), 0);
      --image-block-collage-image-title-color: hsla(var(--black-hsl), 1);
      --image-block-collage-inline-link-color: hsla(var(--black-hsl), 1);
      --image-block-overlap-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-overlap-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-overlap-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-overlap-image-subtitle-color: hsla(var(--black-hsl), 1);
      --image-block-overlap-image-title-bg-color: hsla(var(--white-hsl), 1);
      --image-block-overlap-image-title-color: hsla(var(--black-hsl), 1);
      --image-block-overlap-inline-link-color: hsla(var(--black-hsl), 1);
      --image-block-overlay-color: hsla(var(--black-hsl), 0.5);
      --image-block-poster-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-poster-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-poster-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-poster-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-poster-image-title-bg-color-v2: hsla(var(--white-hsl), 0);
      --image-block-poster-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-poster-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-stack-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-stack-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-stack-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-stack-image-subtitle-color: hsla(var(--black-hsl), 1);
      --image-block-stack-image-title-bg-color: hsla(var(--white-hsl), 0);
      --image-block-stack-image-title-color: hsla(var(--black-hsl), 1);
      --image-block-stack-inline-link-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-arrow-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-arrow-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-banner-slideshow-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-color: hsla(var(--lightAccent-hsl), 1);
      --list-section-banner-slideshow-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-card-description-link-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-title-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-description-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-title-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-arrow-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-arrow-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-carousel-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-carousel-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-carousel-card-color: hsla(var(--lightAccent-hsl), 1);
      --list-section-carousel-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-card-description-link-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-card-title-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-description-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-title-color: hsla(var(--black-hsl), 1);
      --list-section-simple-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-simple-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-simple-card-color: hsla(var(--lightAccent-hsl), 1);
      --list-section-simple-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-simple-card-description-link-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-card-title-color: hsla(var(--black-hsl), 1);
      --list-section-simple-description-color: hsla(var(--black-hsl), 1);
      --list-section-simple-title-color: hsla(var(--black-hsl), 1);
      --list-section-title-color: hsla(var(--black-hsl), 1);
      --menuOverlayBackgroundColor: hsla(var(--white-hsl), 1);
      --menuOverlayButtonBackgroundColor: hsla(var(--safeDarkAccent-hsl), 1);
      --menuOverlayButtonTextColor: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --menuOverlayNavigationLinkColor: hsla(var(--black-hsl), 1);
      --navigationLinkColor: hsla(var(--black-hsl), 1);
      --paragraphLargeColor: hsla(var(--black-hsl), 1);
      --paragraphLinkColor: hsla(var(--safeDarkAccent-hsl), 1);
      --paragraphMediumColor: hsla(var(--black-hsl), 1);
      --paragraphSmallColor: hsla(var(--black-hsl), 1);
      --portfolio-grid-basic-title-color: hsla(var(--black-hsl), 1);
      --portfolio-grid-overlay-overlay-color: hsla(var(--white-hsl), 1);
      --portfolio-grid-overlay-title-color: hsla(var(--black-hsl), 1);
      --portfolio-hover-follow-title-color: hsla(var(--black-hsl), 1);
      --portfolio-hover-static-title-color: hsla(var(--black-hsl), 1);
      --portfolio-index-background-title-color: hsla(var(--black-hsl), 1);
      --primaryButtonBackgroundColor: hsla(var(--safeDarkAccent-hsl), 1);
      --primaryButtonTextColor: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --secondaryButtonBackgroundColor: hsla(var(--safeDarkAccent-hsl), 1);
      --secondaryButtonTextColor: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --section-inset-border-color: hsla(var(--white-hsl), 1);
      --shape-block-background-color: hsla(var(--lightAccent-hsl), 1);
      --shape-block-dropshadow-color: hsla(var(--lightAccent-hsl), 1);
      --siteBackgroundColor: hsla(var(--white-hsl), 1);
      --siteTitleColor: hsla(var(--black-hsl), 1);
      --social-links-block-main-icon-color: hsla(var(--black-hsl), 1);
      --social-links-block-secondary-icon-color: hsla(var(--white-hsl), 1);
      --solidHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --solidHeaderBorderColor: hsla(var(--black-hsl), 1);
      --solidHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --solidHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --summary-block-limited-availability-label-color: hsla(var(--black-hsl), 1);
      --tertiaryButtonBackgroundColor: hsla(var(--safeDarkAccent-hsl), 1);
      --tertiaryButtonTextColor: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --text-highlight-color: hsla(var(--safeDarkAccent-hsl), 1);
      --text-highlight-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-accordion-block-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-accordion-block-divider-color: hsla(var(--black-hsl), 1);
      --tweak-accordion-block-divider-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-accordion-block-icon-color: hsla(var(--black-hsl), 1);
      --tweak-accordion-block-icon-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-title-color: hsla(var(--black-hsl), 1);
      --tweak-blog-basic-grid-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-basic-grid-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-basic-grid-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-basic-grid-list-title-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-author-profile-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-comment-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-comment-text-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-pagination-icon-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-pagination-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-pagination-title-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-title-color: hsla(var(--black-hsl), 1);
      --tweak-blog-masonry-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-masonry-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-masonry-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-masonry-list-title-color: hsla(var(--black-hsl), 1);
      --tweak-blog-side-by-side-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-side-by-side-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-side-by-side-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-side-by-side-list-title-color: hsla(var(--black-hsl), 1);
      --tweak-blog-single-column-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-single-column-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-single-column-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-single-column-list-title-color: hsla(var(--black-hsl), 1);
      --tweak-content-link-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-events-item-pagination-date-color: hsla(var(--black-hsl), 1);
      --tweak-events-item-pagination-icon-color: hsla(var(--black-hsl), 1);
      --tweak-events-item-pagination-title-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-form-block-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-button-background-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-form-block-button-text-color-on-background: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-form-block-caption-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-caption-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-form-block-description-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-description-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-form-block-option-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-option-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-form-block-survey-title-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-survey-title-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-form-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-title-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-gallery-icon-background-color: hsla(var(--white-hsl), 1);
      --tweak-gallery-icon-color: hsla(var(--black-hsl), 1);
      --tweak-gallery-lightbox-background-color: hsla(var(--white-hsl), 1);
      --tweak-gallery-lightbox-icon-color: hsla(var(--black-hsl), 1);
      --tweak-heading-extra-large-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-heading-large-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-heading-medium-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-heading-small-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-line-block-line-color: hsla(var(--black-hsl), 1);
      --tweak-marquee-block-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-marquee-block-heading-color: hsla(var(--black-hsl), 1);
      --tweak-marquee-block-heading-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-marquee-block-paragraph-color: hsla(var(--black-hsl), 1);
      --tweak-marquee-block-paragraph-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-menu-block-item-description-color: hsla(var(--black-hsl), 1);
      --tweak-menu-block-item-price-color: hsla(var(--black-hsl), 1);
      --tweak-menu-block-item-title-color: hsla(var(--black-hsl), 1);
      --tweak-menu-block-nav-color: hsla(var(--black-hsl), 1);
      --tweak-menu-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-newsletter-block-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-button-background-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-newsletter-block-button-text-color-on-background: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-newsletter-block-description-color: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-description-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-footnote-color: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-footnote-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-title-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-paragraph-large-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-paragraph-link-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-paragraph-medium-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-paragraph-small-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-portfolio-item-pagination-icon-color: hsla(var(--black-hsl), 1);
      --tweak-portfolio-item-pagination-meta-color: hsla(var(--black-hsl), 1);
      --tweak-portfolio-item-pagination-title-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-breadcumb-nav-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-description-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-gallery-controls-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-product-basic-item-price-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-scarcity-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-title-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-variant-fields-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-category-nav-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-pagination-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-price-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-scarcity-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-status-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-title-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-button-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-controls-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-overlay-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-quote-block-source-color: hsla(var(--black-hsl), 1);
      --tweak-quote-block-source-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-quote-block-text-color: hsla(var(--black-hsl), 1);
      --tweak-quote-block-text-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-summary-block-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-excerpt-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-header-text-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-header-text-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-primary-metadata-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-primary-metadata-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-read-more-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-read-more-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-secondary-metadata-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-secondary-metadata-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-title-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-text-block-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-video-item-description-color: hsla(var(--black-hsl), 1);
      --tweak-video-item-meta-color: hsla(var(--black-hsl), 1);
      --tweak-video-item-pagination-icon-color: hsla(var(--black-hsl), 1);
      --tweak-video-item-pagination-title-color: hsla(var(--black-hsl), 1);
      --tweak-video-item-title-color: hsla(var(--black-hsl), 1);
      --video-grid-basic-description-color: hsla(var(--black-hsl), 1);
      --video-grid-basic-meta-color: hsla(var(--black-hsl), 1);
      --video-grid-basic-title-color: hsla(var(--black-hsl), 1);
      --video-grid-category-nav-color: hsla(var(--black-hsl), 1);
    }

    .white-bold {
      --announcement-bar-background-color: hsla(var(--accent-hsl), 1);
      --announcement-bar-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --backgroundOverlayColor: hsla(var(--white-hsl), 1);
      --course-item-nav-active-lesson-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --course-item-nav-active-lesson-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --course-item-nav-background-color: hsla(var(--white-hsl), 1);
      --course-item-nav-border-color: hsla(var(--black-hsl), 0.25);
      --course-item-nav-open-tab-background-color: hsla(var(--black-hsl), 0.75);
      --course-item-nav-open-tab-text-color: hsla(var(--white-hsl), 1);
      --course-item-nav-text-color: hsla(var(--black-hsl), 1);
      --course-list-course-item-background: hsla(var(--lightAccent-hsl), 1);
      --course-list-course-item-hover-background: hsla(var(--lightAccent-hsl), 0.75);
      --course-list-course-item-text-color: hsla(var(--black-hsl), 1);
      --course-list-course-progress-bar-color: hsla(var(--safeDarkAccent-hsl), 1);
      --gradientHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --gradientHeaderBorderColor: hsla(var(--black-hsl), 1);
      --gradientHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --gradientHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --headingExtraLargeColor: hsla(var(--safeDarkAccent-hsl), 1);
      --headingLargeColor: hsla(var(--safeDarkAccent-hsl), 1);
      --headingLinkColor: hsla(var(--safeDarkAccent-hsl), 1);
      --headingMediumColor: hsla(var(--safeDarkAccent-hsl), 1);
      --headingSmallColor: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-card-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-card-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-card-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-card-image-subtitle-color: hsla(var(--black-hsl), 1);
      --image-block-card-image-title-bg-color: hsla(var(--white-hsl), 0);
      --image-block-card-image-title-color: hsla(var(--black-hsl), 1);
      --image-block-card-inline-link-color: hsla(var(--black-hsl), 1);
      --image-block-collage-background-color: hsla(var(--lightAccent-hsl), 1);
      --image-block-collage-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-collage-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-collage-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-collage-image-subtitle-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-collage-image-title-bg-color: hsla(var(--white-hsl), 0);
      --image-block-collage-image-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-collage-inline-link-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-overlap-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-overlap-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-overlap-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-overlap-image-subtitle-color: hsla(var(--black-hsl), 1);
      --image-block-overlap-image-title-bg-color: hsla(var(--white-hsl), 1);
      --image-block-overlap-image-title-color: hsla(var(--black-hsl), 1);
      --image-block-overlap-inline-link-color: hsla(var(--black-hsl), 1);
      --image-block-overlay-color: hsla(var(--black-hsl), 0.5);
      --image-block-poster-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-poster-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-poster-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-poster-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-poster-image-title-bg-color-v2: hsla(var(--white-hsl), 0);
      --image-block-poster-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-poster-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-stack-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-stack-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-stack-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-stack-image-subtitle-color: hsla(var(--black-hsl), 1);
      --image-block-stack-image-title-bg-color: hsla(var(--white-hsl), 0);
      --image-block-stack-image-title-color: hsla(var(--black-hsl), 1);
      --image-block-stack-inline-link-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-arrow-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-arrow-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-banner-slideshow-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-color: hsla(var(--lightAccent-hsl), 1);
      --list-section-banner-slideshow-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-card-description-link-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-description-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-arrow-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-arrow-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-carousel-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-carousel-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-carousel-card-color: hsla(var(--lightAccent-hsl), 1);
      --list-section-carousel-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-card-description-link-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-card-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-description-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-simple-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-simple-card-color: hsla(var(--lightAccent-hsl), 1);
      --list-section-simple-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-simple-card-description-link-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-card-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-description-color: hsla(var(--black-hsl), 1);
      --list-section-simple-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --menuOverlayBackgroundColor: hsla(var(--white-hsl), 1);
      --menuOverlayButtonBackgroundColor: hsla(var(--safeDarkAccent-hsl), 1);
      --menuOverlayButtonTextColor: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --menuOverlayNavigationLinkColor: hsla(var(--black-hsl), 1);
      --navigationLinkColor: hsla(var(--black-hsl), 1);
      --paragraphLargeColor: hsla(var(--black-hsl), 1);
      --paragraphLinkColor: hsla(var(--safeDarkAccent-hsl), 1);
      --paragraphMediumColor: hsla(var(--black-hsl), 1);
      --paragraphSmallColor: hsla(var(--black-hsl), 1);
      --portfolio-grid-basic-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --portfolio-grid-overlay-overlay-color: hsla(var(--white-hsl), 1);
      --portfolio-grid-overlay-title-color: hsla(var(--black-hsl), 1);
      --portfolio-hover-follow-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --portfolio-hover-static-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --portfolio-index-background-title-color: hsla(var(--black-hsl), 1);
      --primaryButtonBackgroundColor: hsla(var(--safeDarkAccent-hsl), 1);
      --primaryButtonTextColor: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --secondaryButtonBackgroundColor: hsla(var(--safeDarkAccent-hsl), 1);
      --secondaryButtonTextColor: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --section-inset-border-color: hsla(var(--white-hsl), 1);
      --shape-block-background-color: hsla(var(--lightAccent-hsl), 1);
      --shape-block-dropshadow-color: hsla(var(--lightAccent-hsl), 1);
      --siteBackgroundColor: hsla(var(--white-hsl), 1);
      --siteTitleColor: hsla(var(--safeDarkAccent-hsl), 1);
      --social-links-block-main-icon-color: hsla(var(--black-hsl), 1);
      --social-links-block-secondary-icon-color: hsla(var(--white-hsl), 1);
      --solidHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --solidHeaderBorderColor: hsla(var(--black-hsl), 1);
      --solidHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --solidHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --summary-block-limited-availability-label-color: hsla(var(--black-hsl), 1);
      --tertiaryButtonBackgroundColor: hsla(var(--safeDarkAccent-hsl), 1);
      --tertiaryButtonTextColor: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --text-highlight-color: hsla(var(--safeDarkAccent-hsl), 1);
      --text-highlight-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-accordion-block-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-accordion-block-divider-color: hsla(var(--black-hsl), 1);
      --tweak-accordion-block-divider-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-accordion-block-icon-color: hsla(var(--black-hsl), 1);
      --tweak-accordion-block-icon-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-basic-grid-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-basic-grid-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-basic-grid-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-basic-grid-list-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-item-author-profile-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-comment-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-comment-text-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-pagination-icon-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-pagination-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-pagination-title-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-masonry-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-masonry-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-masonry-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-masonry-list-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-side-by-side-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-side-by-side-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-side-by-side-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-side-by-side-list-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-single-column-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-single-column-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-single-column-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-single-column-list-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-content-link-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-events-item-pagination-date-color: hsla(var(--black-hsl), 1);
      --tweak-events-item-pagination-icon-color: hsla(var(--black-hsl), 1);
      --tweak-events-item-pagination-title-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-form-block-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-button-background-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-form-block-button-text-color-on-background: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-form-block-caption-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-caption-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-description-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-description-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-option-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-option-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-survey-title-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-survey-title-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-title-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-gallery-icon-background-color: hsla(var(--white-hsl), 1);
      --tweak-gallery-icon-color: hsla(var(--black-hsl), 1);
      --tweak-gallery-lightbox-background-color: hsla(var(--white-hsl), 1);
      --tweak-gallery-lightbox-icon-color: hsla(var(--black-hsl), 1);
      --tweak-heading-extra-large-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-heading-large-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-heading-medium-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-heading-small-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-line-block-line-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-marquee-block-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-marquee-block-heading-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-marquee-block-heading-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-marquee-block-paragraph-color: hsla(var(--black-hsl), 1);
      --tweak-marquee-block-paragraph-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-menu-block-item-description-color: hsla(var(--black-hsl), 1);
      --tweak-menu-block-item-price-color: hsla(var(--black-hsl), 1);
      --tweak-menu-block-item-title-color: hsla(var(--black-hsl), 1);
      --tweak-menu-block-nav-color: hsla(var(--black-hsl), 1);
      --tweak-menu-block-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-newsletter-block-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-button-background-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-newsletter-block-button-text-color-on-background: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-newsletter-block-description-color: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-description-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-footnote-color: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-footnote-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-title-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-paragraph-large-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-paragraph-link-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-paragraph-medium-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-paragraph-small-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-portfolio-item-pagination-icon-color: hsla(var(--black-hsl), 1);
      --tweak-portfolio-item-pagination-meta-color: hsla(var(--black-hsl), 1);
      --tweak-portfolio-item-pagination-title-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-breadcumb-nav-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-description-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-gallery-controls-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-product-basic-item-price-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-scarcity-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-product-basic-item-variant-fields-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-category-nav-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-pagination-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-price-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-scarcity-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-status-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-product-quick-view-button-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-controls-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-overlay-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-quote-block-source-color: hsla(var(--black-hsl), 1);
      --tweak-quote-block-source-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-quote-block-text-color: hsla(var(--black-hsl), 1);
      --tweak-quote-block-text-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-summary-block-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-excerpt-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-header-text-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-header-text-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-primary-metadata-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-primary-metadata-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-read-more-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-read-more-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-secondary-metadata-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-secondary-metadata-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-title-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-text-block-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-video-item-description-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-meta-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-pagination-icon-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-pagination-title-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-title-color: hsla(var(--accent-hsl), 1);
      --video-grid-basic-description-color: hsla(var(--accent-hsl), 1);
      --video-grid-basic-meta-color: hsla(var(--accent-hsl), 1);
      --video-grid-basic-title-color: hsla(var(--accent-hsl), 1);
      --video-grid-category-nav-color: hsla(var(--accent-hsl), 1);
    }

    .light {
      --announcement-bar-background-color: hsla(var(--darkAccent-hsl), 1);
      --announcement-bar-text-color: hsla(var(--white-hsl), 1);
      --backgroundOverlayColor: hsla(var(--lightAccent-hsl), 1);
      --course-item-nav-active-lesson-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --course-item-nav-active-lesson-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --course-item-nav-background-color: hsla(var(--white-hsl), 1);
      --course-item-nav-border-color: hsla(var(--black-hsl), 0.25);
      --course-item-nav-open-tab-background-color: hsla(var(--black-hsl), 0.75);
      --course-item-nav-open-tab-text-color: hsla(var(--white-hsl), 1);
      --course-item-nav-text-color: hsla(var(--black-hsl), 1);
      --course-list-course-item-background: hsla(var(--white-hsl), 1);
      --course-list-course-item-hover-background: hsla(var(--white-hsl), 0.75);
      --course-list-course-item-text-color: hsla(var(--black-hsl), 1);
      --course-list-course-progress-bar-color: hsla(var(--black-hsl), 1);
      --gradientHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --gradientHeaderBorderColor: hsla(var(--black-hsl), 1);
      --gradientHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --gradientHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --headingExtraLargeColor: hsla(var(--black-hsl), 1);
      --headingLargeColor: hsla(var(--black-hsl), 1);
      --headingLinkColor: hsla(var(--safeDarkAccent-hsl), 1);
      --headingMediumColor: hsla(var(--black-hsl), 1);
      --headingSmallColor: hsla(var(--black-hsl), 1);
      --image-block-card-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-card-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-card-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-card-image-subtitle-color: hsla(var(--black-hsl), 1);
      --image-block-card-image-title-bg-color: hsla(var(--lightAccent-hsl), 0);
      --image-block-card-image-title-color: hsla(var(--black-hsl), 1);
      --image-block-card-inline-link-color: hsla(var(--black-hsl), 1);
      --image-block-collage-background-color: hsla(var(--white-hsl), 1);
      --image-block-collage-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-collage-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-collage-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-collage-image-subtitle-color: hsla(var(--black-hsl), 1);
      --image-block-collage-image-title-bg-color: hsla(var(--lightAccent-hsl), 0);
      --image-block-collage-image-title-color: hsla(var(--black-hsl), 1);
      --image-block-collage-inline-link-color: hsla(var(--black-hsl), 1);
      --image-block-overlap-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-overlap-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-overlap-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-overlap-image-subtitle-color: hsla(var(--black-hsl), 1);
      --image-block-overlap-image-title-bg-color: hsla(var(--lightAccent-hsl), 1);
      --image-block-overlap-image-title-color: hsla(var(--black-hsl), 1);
      --image-block-overlap-inline-link-color: hsla(var(--black-hsl), 1);
      --image-block-overlay-color: hsla(var(--black-hsl), 0.5);
      --image-block-poster-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-poster-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-poster-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-poster-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-poster-image-title-bg-color-v2: hsla(var(--lightAccent-hsl), 0);
      --image-block-poster-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-poster-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-stack-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-stack-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-stack-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-stack-image-subtitle-color: hsla(var(--black-hsl), 1);
      --image-block-stack-image-title-bg-color: hsla(var(--lightAccent-hsl), 0);
      --image-block-stack-image-title-color: hsla(var(--black-hsl), 1);
      --image-block-stack-inline-link-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-arrow-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-arrow-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-banner-slideshow-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-color: hsla(var(--white-hsl), 1);
      --list-section-banner-slideshow-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-card-description-link-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-title-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-description-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-title-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-arrow-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-arrow-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-carousel-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-carousel-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-carousel-card-color: hsla(var(--white-hsl), 1);
      --list-section-carousel-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-card-description-link-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-card-title-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-description-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-title-color: hsla(var(--black-hsl), 1);
      --list-section-simple-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-simple-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-simple-card-color: hsla(var(--white-hsl), 1);
      --list-section-simple-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-simple-card-description-link-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-card-title-color: hsla(var(--black-hsl), 1);
      --list-section-simple-description-color: hsla(var(--black-hsl), 1);
      --list-section-simple-title-color: hsla(var(--black-hsl), 1);
      --list-section-title-color: hsla(var(--black-hsl), 1);
      --menuOverlayBackgroundColor: hsla(var(--lightAccent-hsl), 1);
      --menuOverlayButtonBackgroundColor: hsla(var(--safeDarkAccent-hsl), 1);
      --menuOverlayButtonTextColor: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --menuOverlayNavigationLinkColor: hsla(var(--black-hsl), 1);
      --navigationLinkColor: hsla(var(--black-hsl), 1);
      --paragraphLargeColor: hsla(var(--black-hsl), 1);
      --paragraphLinkColor: hsla(var(--safeDarkAccent-hsl), 1);
      --paragraphMediumColor: hsla(var(--black-hsl), 1);
      --paragraphSmallColor: hsla(var(--black-hsl), 1);
      --portfolio-grid-basic-title-color: hsla(var(--black-hsl), 1);
      --portfolio-grid-overlay-overlay-color: hsla(var(--lightAccent-hsl), 1);
      --portfolio-grid-overlay-title-color: hsla(var(--black-hsl), 1);
      --portfolio-hover-follow-title-color: hsla(var(--black-hsl), 1);
      --portfolio-hover-static-title-color: hsla(var(--black-hsl), 1);
      --portfolio-index-background-title-color: hsla(var(--black-hsl), 1);
      --primaryButtonBackgroundColor: hsla(var(--safeDarkAccent-hsl), 1);
      --primaryButtonTextColor: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --secondaryButtonBackgroundColor: hsla(var(--safeDarkAccent-hsl), 1);
      --secondaryButtonTextColor: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --section-inset-border-color: hsla(var(--white-hsl), 1);
      --shape-block-background-color: hsla(var(--white-hsl), 1);
      --shape-block-dropshadow-color: hsla(var(--white-hsl), 1);
      --siteBackgroundColor: hsla(var(--lightAccent-hsl), 1);
      --siteTitleColor: hsla(var(--black-hsl), 1);
      --social-links-block-main-icon-color: hsla(var(--black-hsl), 1);
      --social-links-block-secondary-icon-color: hsla(var(--lightAccent-hsl), 1);
      --solidHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --solidHeaderBorderColor: hsla(var(--black-hsl), 1);
      --solidHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --solidHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --summary-block-limited-availability-label-color: hsla(var(--black-hsl), 1);
      --tertiaryButtonBackgroundColor: hsla(var(--safeDarkAccent-hsl), 1);
      --tertiaryButtonTextColor: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --text-highlight-color: hsla(var(--safeDarkAccent-hsl), 1);
      --text-highlight-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-accordion-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-accordion-block-divider-color: hsla(var(--black-hsl), 1);
      --tweak-accordion-block-divider-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-accordion-block-icon-color: hsla(var(--black-hsl), 1);
      --tweak-accordion-block-icon-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-title-color: hsla(var(--black-hsl), 1);
      --tweak-blog-basic-grid-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-basic-grid-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-basic-grid-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-basic-grid-list-title-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-author-profile-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-comment-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-comment-text-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-pagination-icon-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-pagination-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-pagination-title-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-title-color: hsla(var(--black-hsl), 1);
      --tweak-blog-masonry-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-masonry-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-masonry-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-masonry-list-title-color: hsla(var(--black-hsl), 1);
      --tweak-blog-side-by-side-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-side-by-side-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-side-by-side-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-side-by-side-list-title-color: hsla(var(--black-hsl), 1);
      --tweak-blog-single-column-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-single-column-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-single-column-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-single-column-list-title-color: hsla(var(--black-hsl), 1);
      --tweak-content-link-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-events-item-pagination-date-color: hsla(var(--black-hsl), 1);
      --tweak-events-item-pagination-icon-color: hsla(var(--black-hsl), 1);
      --tweak-events-item-pagination-title-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-button-background-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-form-block-button-text-color-on-background: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-form-block-caption-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-caption-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-form-block-description-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-description-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-form-block-option-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-option-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-form-block-survey-title-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-survey-title-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-form-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-title-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-gallery-icon-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-gallery-icon-color: hsla(var(--black-hsl), 1);
      --tweak-gallery-lightbox-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-gallery-lightbox-icon-color: hsla(var(--black-hsl), 1);
      --tweak-heading-extra-large-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-heading-large-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-heading-medium-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-heading-small-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-line-block-line-color: hsla(var(--black-hsl), 1);
      --tweak-marquee-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-marquee-block-heading-color: hsla(var(--black-hsl), 1);
      --tweak-marquee-block-heading-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-marquee-block-paragraph-color: hsla(var(--black-hsl), 1);
      --tweak-marquee-block-paragraph-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-menu-block-item-description-color: hsla(var(--black-hsl), 1);
      --tweak-menu-block-item-price-color: hsla(var(--black-hsl), 1);
      --tweak-menu-block-item-title-color: hsla(var(--black-hsl), 1);
      --tweak-menu-block-nav-color: hsla(var(--black-hsl), 1);
      --tweak-menu-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-button-background-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-newsletter-block-button-text-color-on-background: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-newsletter-block-description-color: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-description-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-footnote-color: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-footnote-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-title-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-paragraph-large-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-paragraph-link-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-paragraph-medium-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-paragraph-small-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-portfolio-item-pagination-icon-color: hsla(var(--black-hsl), 1);
      --tweak-portfolio-item-pagination-meta-color: hsla(var(--black-hsl), 1);
      --tweak-portfolio-item-pagination-title-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-breadcumb-nav-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-description-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-gallery-controls-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-product-basic-item-price-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-scarcity-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-title-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-variant-fields-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-category-nav-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-pagination-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-price-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-scarcity-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-status-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-title-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-button-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-controls-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-overlay-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-source-color: hsla(var(--black-hsl), 1);
      --tweak-quote-block-source-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-quote-block-text-color: hsla(var(--black-hsl), 1);
      --tweak-quote-block-text-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-excerpt-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-header-text-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-header-text-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-primary-metadata-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-primary-metadata-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-read-more-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-read-more-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-secondary-metadata-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-secondary-metadata-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-title-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-text-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-description-color: hsla(var(--black-hsl), 1);
      --tweak-video-item-meta-color: hsla(var(--black-hsl), 1);
      --tweak-video-item-pagination-icon-color: hsla(var(--black-hsl), 1);
      --tweak-video-item-pagination-title-color: hsla(var(--black-hsl), 1);
      --tweak-video-item-title-color: hsla(var(--black-hsl), 1);
      --video-grid-basic-description-color: hsla(var(--black-hsl), 1);
      --video-grid-basic-meta-color: hsla(var(--black-hsl), 1);
      --video-grid-basic-title-color: hsla(var(--black-hsl), 1);
      --video-grid-category-nav-color: hsla(var(--black-hsl), 1);
    }

    .light-bold {
      --announcement-bar-background-color: hsla(var(--accent-hsl), 1);
      --announcement-bar-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --backgroundOverlayColor: hsla(var(--lightAccent-hsl), 1);
      --course-item-nav-active-lesson-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --course-item-nav-active-lesson-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --course-item-nav-background-color: hsla(var(--white-hsl), 1);
      --course-item-nav-border-color: hsla(var(--black-hsl), 0.25);
      --course-item-nav-open-tab-background-color: hsla(var(--black-hsl), 0.75);
      --course-item-nav-open-tab-text-color: hsla(var(--white-hsl), 1);
      --course-item-nav-text-color: hsla(var(--black-hsl), 1);
      --course-list-course-item-background: hsla(var(--white-hsl), 1);
      --course-list-course-item-hover-background: hsla(var(--white-hsl), 0.75);
      --course-list-course-item-text-color: hsla(var(--black-hsl), 1);
      --course-list-course-progress-bar-color: hsla(var(--safeDarkAccent-hsl), 1);
      --gradientHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --gradientHeaderBorderColor: hsla(var(--black-hsl), 1);
      --gradientHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --gradientHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --headingExtraLargeColor: hsla(var(--safeDarkAccent-hsl), 1);
      --headingLargeColor: hsla(var(--safeDarkAccent-hsl), 1);
      --headingLinkColor: hsla(var(--safeDarkAccent-hsl), 1);
      --headingMediumColor: hsla(var(--safeDarkAccent-hsl), 1);
      --headingSmallColor: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-card-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-card-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-card-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-card-image-subtitle-color: hsla(var(--black-hsl), 1);
      --image-block-card-image-title-bg-color: hsla(var(--lightAccent-hsl), 0);
      --image-block-card-image-title-color: hsla(var(--black-hsl), 1);
      --image-block-card-inline-link-color: hsla(var(--black-hsl), 1);
      --image-block-collage-background-color: hsla(var(--white-hsl), 1);
      --image-block-collage-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-collage-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-collage-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-collage-image-subtitle-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-collage-image-title-bg-color: hsla(var(--lightAccent-hsl), 0);
      --image-block-collage-image-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-collage-inline-link-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-overlap-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-overlap-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-overlap-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-overlap-image-subtitle-color: hsla(var(--black-hsl), 1);
      --image-block-overlap-image-title-bg-color: hsla(var(--lightAccent-hsl), 1);
      --image-block-overlap-image-title-color: hsla(var(--black-hsl), 1);
      --image-block-overlap-inline-link-color: hsla(var(--black-hsl), 1);
      --image-block-overlay-color: hsla(var(--black-hsl), 0.5);
      --image-block-poster-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-poster-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-poster-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-poster-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-poster-image-title-bg-color-v2: hsla(var(--lightAccent-hsl), 0);
      --image-block-poster-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-poster-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-stack-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-stack-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-stack-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-stack-image-subtitle-color: hsla(var(--black-hsl), 1);
      --image-block-stack-image-title-bg-color: hsla(var(--lightAccent-hsl), 0);
      --image-block-stack-image-title-color: hsla(var(--black-hsl), 1);
      --image-block-stack-inline-link-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-arrow-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-arrow-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-banner-slideshow-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-color: hsla(var(--white-hsl), 1);
      --list-section-banner-slideshow-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-card-description-link-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-description-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-arrow-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-arrow-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-carousel-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-carousel-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-carousel-card-color: hsla(var(--white-hsl), 1);
      --list-section-carousel-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-card-description-link-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-card-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-description-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-simple-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-simple-card-color: hsla(var(--white-hsl), 1);
      --list-section-simple-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-simple-card-description-link-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-card-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-description-color: hsla(var(--black-hsl), 1);
      --list-section-simple-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --menuOverlayBackgroundColor: hsla(var(--lightAccent-hsl), 1);
      --menuOverlayButtonBackgroundColor: hsla(var(--safeDarkAccent-hsl), 1);
      --menuOverlayButtonTextColor: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --menuOverlayNavigationLinkColor: hsla(var(--black-hsl), 1);
      --navigationLinkColor: hsla(var(--black-hsl), 1);
      --paragraphLargeColor: hsla(var(--black-hsl), 1);
      --paragraphLinkColor: hsla(var(--safeDarkAccent-hsl), 1);
      --paragraphMediumColor: hsla(var(--black-hsl), 1);
      --paragraphSmallColor: hsla(var(--black-hsl), 1);
      --portfolio-grid-basic-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --portfolio-grid-overlay-overlay-color: hsla(var(--lightAccent-hsl), 1);
      --portfolio-grid-overlay-title-color: hsla(var(--black-hsl), 1);
      --portfolio-hover-follow-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --portfolio-hover-static-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --portfolio-index-background-title-color: hsla(var(--black-hsl), 1);
      --primaryButtonBackgroundColor: hsla(var(--safeDarkAccent-hsl), 1);
      --primaryButtonTextColor: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --secondaryButtonBackgroundColor: hsla(var(--safeDarkAccent-hsl), 1);
      --secondaryButtonTextColor: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --section-inset-border-color: hsla(var(--white-hsl), 1);
      --shape-block-background-color: hsla(var(--white-hsl), 1);
      --shape-block-dropshadow-color: hsla(var(--white-hsl), 1);
      --siteBackgroundColor: hsla(var(--lightAccent-hsl), 1);
      --siteTitleColor: hsla(var(--safeDarkAccent-hsl), 1);
      --social-links-block-main-icon-color: hsla(var(--black-hsl), 1);
      --social-links-block-secondary-icon-color: hsla(var(--lightAccent-hsl), 1);
      --solidHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --solidHeaderBorderColor: hsla(var(--black-hsl), 1);
      --solidHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --solidHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --summary-block-limited-availability-label-color: hsla(var(--black-hsl), 1);
      --tertiaryButtonBackgroundColor: hsla(var(--safeDarkAccent-hsl), 1);
      --tertiaryButtonTextColor: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --text-highlight-color: hsla(var(--safeDarkAccent-hsl), 1);
      --text-highlight-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-accordion-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-accordion-block-divider-color: hsla(var(--black-hsl), 1);
      --tweak-accordion-block-divider-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-accordion-block-icon-color: hsla(var(--black-hsl), 1);
      --tweak-accordion-block-icon-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-basic-grid-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-basic-grid-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-basic-grid-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-basic-grid-list-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-item-author-profile-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-comment-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-comment-text-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-pagination-icon-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-pagination-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-pagination-title-color: hsla(var(--black-hsl), 1);
      --tweak-blog-item-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-masonry-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-masonry-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-masonry-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-masonry-list-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-side-by-side-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-side-by-side-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-side-by-side-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-side-by-side-list-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-single-column-list-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-blog-single-column-list-meta-color: hsla(var(--black-hsl), 1);
      --tweak-blog-single-column-list-read-more-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-single-column-list-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-content-link-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-events-item-pagination-date-color: hsla(var(--black-hsl), 1);
      --tweak-events-item-pagination-icon-color: hsla(var(--black-hsl), 1);
      --tweak-events-item-pagination-title-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-button-background-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-form-block-button-text-color-on-background: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-form-block-caption-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-caption-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-description-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-description-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-option-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-option-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-survey-title-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-survey-title-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-form-block-title-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-gallery-icon-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-gallery-icon-color: hsla(var(--black-hsl), 1);
      --tweak-gallery-lightbox-background-color: hsla(var(--lightAccent-hsl), 1);
      --tweak-gallery-lightbox-icon-color: hsla(var(--black-hsl), 1);
      --tweak-heading-extra-large-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-heading-large-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-heading-medium-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-heading-small-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-line-block-line-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-marquee-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-marquee-block-heading-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-marquee-block-heading-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-marquee-block-paragraph-color: hsla(var(--black-hsl), 1);
      --tweak-marquee-block-paragraph-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-menu-block-item-description-color: hsla(var(--black-hsl), 1);
      --tweak-menu-block-item-price-color: hsla(var(--black-hsl), 1);
      --tweak-menu-block-item-title-color: hsla(var(--black-hsl), 1);
      --tweak-menu-block-nav-color: hsla(var(--black-hsl), 1);
      --tweak-menu-block-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-button-background-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-newsletter-block-button-text-color-on-background: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-newsletter-block-description-color: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-description-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-footnote-color: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-footnote-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-title-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-paragraph-large-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-paragraph-link-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-paragraph-medium-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-paragraph-small-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-portfolio-item-pagination-icon-color: hsla(var(--black-hsl), 1);
      --tweak-portfolio-item-pagination-meta-color: hsla(var(--black-hsl), 1);
      --tweak-portfolio-item-pagination-title-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-breadcumb-nav-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-description-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-gallery-controls-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-product-basic-item-price-color: hsla(var(--black-hsl), 1);
      --tweak-product-basic-item-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-scarcity-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-product-basic-item-variant-fields-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-category-nav-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-pagination-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-price-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-scarcity-color: hsla(var(--black-hsl), 1);
      --tweak-product-grid-text-below-list-status-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-product-quick-view-button-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-controls-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-overlay-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-source-color: hsla(var(--black-hsl), 1);
      --tweak-quote-block-source-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-quote-block-text-color: hsla(var(--black-hsl), 1);
      --tweak-quote-block-text-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-excerpt-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-excerpt-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-header-text-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-header-text-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-primary-metadata-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-primary-metadata-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-read-more-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-read-more-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-secondary-metadata-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-secondary-metadata-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-title-color: hsla(var(--black-hsl), 1);
      --tweak-summary-block-title-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-text-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-description-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-meta-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-pagination-icon-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-pagination-title-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-title-color: hsla(var(--accent-hsl), 1);
      --video-grid-basic-description-color: hsla(var(--accent-hsl), 1);
      --video-grid-basic-meta-color: hsla(var(--accent-hsl), 1);
      --video-grid-basic-title-color: hsla(var(--accent-hsl), 1);
      --video-grid-category-nav-color: hsla(var(--accent-hsl), 1);
    }

    .dark {
      --announcement-bar-background-color: hsla(var(--lightAccent-hsl), 1);
      --announcement-bar-text-color: hsla(var(--black-hsl), 1);
      --backgroundOverlayColor: hsla(var(--darkAccent-hsl), 1);
      --course-item-nav-active-lesson-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --course-item-nav-active-lesson-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --course-item-nav-background-color: hsla(var(--black-hsl), 1);
      --course-item-nav-border-color: hsla(var(--white-hsl), 0.25);
      --course-item-nav-open-tab-background-color: hsla(var(--white-hsl), 0.75);
      --course-item-nav-open-tab-text-color: hsla(var(--black-hsl), 1);
      --course-item-nav-text-color: hsla(var(--white-hsl), 1);
      --course-list-course-item-background: hsla(var(--white-hsl), 1);
      --course-list-course-item-hover-background: hsla(var(--white-hsl), 0.9);
      --course-list-course-item-text-color: hsla(var(--black-hsl), 1);
      --course-list-course-progress-bar-color: hsla(var(--accent-hsl), 1);
      --gradientHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --gradientHeaderBorderColor: hsla(var(--black-hsl), 1);
      --gradientHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --gradientHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --headingExtraLargeColor: hsla(var(--white-hsl), 1);
      --headingLargeColor: hsla(var(--white-hsl), 1);
      --headingLinkColor: hsla(var(--lightAccent-hsl), 1);
      --headingMediumColor: hsla(var(--white-hsl), 1);
      --headingSmallColor: hsla(var(--white-hsl), 1);
      --image-block-card-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-card-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-card-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-card-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-card-image-title-bg-color: hsla(var(--darkAccent-hsl), 0);
      --image-block-card-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-card-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-collage-background-color: hsla(var(--white-hsl), 1);
      --image-block-collage-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-collage-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-collage-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-collage-image-subtitle-color: hsla(var(--black-hsl), 1);
      --image-block-collage-image-title-bg-color: hsla(var(--darkAccent-hsl), 0);
      --image-block-collage-image-title-color: hsla(var(--black-hsl), 1);
      --image-block-collage-inline-link-color: hsla(var(--black-hsl), 1);
      --image-block-overlap-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-overlap-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-overlap-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-overlap-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-overlap-image-title-bg-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-overlap-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-overlap-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-overlay-color: hsla(var(--black-hsl), 0.5);
      --image-block-poster-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-poster-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-poster-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-poster-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-poster-image-title-bg-color-v2: hsla(var(--darkAccent-hsl), 0);
      --image-block-poster-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-poster-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-stack-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-stack-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-stack-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-stack-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-stack-image-title-bg-color: hsla(var(--darkAccent-hsl), 0);
      --image-block-stack-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-stack-inline-link-color: hsla(var(--white-hsl), 1);
      --list-section-banner-slideshow-arrow-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-banner-slideshow-arrow-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-banner-slideshow-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-banner-slideshow-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-color: hsla(var(--white-hsl), 1);
      --list-section-banner-slideshow-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-card-description-link-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-banner-slideshow-card-title-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-description-color: hsla(var(--white-hsl), 1);
      --list-section-banner-slideshow-title-color: hsla(var(--white-hsl), 1);
      --list-section-carousel-arrow-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-carousel-arrow-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-carousel-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-carousel-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-carousel-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-carousel-card-color: hsla(var(--white-hsl), 1);
      --list-section-carousel-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-card-description-link-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-carousel-card-title-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-description-color: hsla(var(--white-hsl), 1);
      --list-section-carousel-title-color: hsla(var(--white-hsl), 1);
      --list-section-simple-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-simple-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-simple-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-simple-card-color: hsla(var(--white-hsl), 1);
      --list-section-simple-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-simple-card-description-link-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-simple-card-title-color: hsla(var(--black-hsl), 1);
      --list-section-simple-description-color: hsla(var(--white-hsl), 1);
      --list-section-simple-title-color: hsla(var(--white-hsl), 1);
      --list-section-title-color: hsla(var(--white-hsl), 1);
      --menuOverlayBackgroundColor: hsla(var(--darkAccent-hsl), 1);
      --menuOverlayButtonBackgroundColor: hsla(var(--safeLightAccent-hsl), 1);
      --menuOverlayButtonTextColor: hsla(var(--safeInverseLightAccent-hsl), 1);
      --menuOverlayNavigationLinkColor: hsla(var(--white-hsl), 1);
      --navigationLinkColor: hsla(var(--white-hsl), 1);
      --paragraphLargeColor: hsla(var(--white-hsl), 1);
      --paragraphLinkColor: hsla(var(--safeLightAccent-hsl), 1);
      --paragraphMediumColor: hsla(var(--white-hsl), 1);
      --paragraphSmallColor: hsla(var(--white-hsl), 1);
      --portfolio-grid-basic-title-color: hsla(var(--white-hsl), 1);
      --portfolio-grid-overlay-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --portfolio-grid-overlay-title-color: hsla(var(--white-hsl), 1);
      --portfolio-hover-follow-title-color: hsla(var(--white-hsl), 1);
      --portfolio-hover-static-title-color: hsla(var(--white-hsl), 1);
      --portfolio-index-background-title-color: hsla(var(--white-hsl), 1);
      --primaryButtonBackgroundColor: hsla(var(--safeLightAccent-hsl), 1);
      --primaryButtonTextColor: hsla(var(--safeInverseLightAccent-hsl), 1);
      --secondaryButtonBackgroundColor: hsla(var(--safeLightAccent-hsl), 1);
      --secondaryButtonTextColor: hsla(var(--safeInverseLightAccent-hsl), 1);
      --section-inset-border-color: hsla(var(--white-hsl), 1);
      --shape-block-background-color: hsla(var(--white-hsl), 1);
      --shape-block-dropshadow-color: hsla(var(--white-hsl), 1);
      --siteBackgroundColor: hsla(var(--darkAccent-hsl), 1);
      --siteTitleColor: hsla(var(--white-hsl), 1);
      --social-links-block-main-icon-color: hsla(var(--white-hsl), 1);
      --social-links-block-secondary-icon-color: hsla(var(--darkAccent-hsl), 1);
      --solidHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --solidHeaderBorderColor: hsla(var(--black-hsl), 1);
      --solidHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --solidHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --summary-block-limited-availability-label-color: hsla(var(--white-hsl), 1);
      --tertiaryButtonBackgroundColor: hsla(var(--safeLightAccent-hsl), 1);
      --tertiaryButtonTextColor: hsla(var(--safeInverseLightAccent-hsl), 1);
      --text-highlight-color: hsla(var(--safeLightAccent-hsl), 1);
      --text-highlight-color-on-background: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-accordion-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-accordion-block-divider-color: hsla(var(--white-hsl), 1);
      --tweak-accordion-block-divider-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-accordion-block-icon-color: hsla(var(--white-hsl), 1);
      --tweak-accordion-block-icon-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-title-color: hsla(var(--white-hsl), 1);
      --tweak-blog-basic-grid-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-basic-grid-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-basic-grid-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-basic-grid-list-title-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-author-profile-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-comment-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-comment-text-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-pagination-icon-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-pagination-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-pagination-title-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-title-color: hsla(var(--white-hsl), 1);
      --tweak-blog-masonry-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-masonry-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-masonry-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-masonry-list-title-color: hsla(var(--white-hsl), 1);
      --tweak-blog-side-by-side-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-side-by-side-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-side-by-side-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-side-by-side-list-title-color: hsla(var(--white-hsl), 1);
      --tweak-blog-single-column-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-single-column-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-single-column-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-single-column-list-title-color: hsla(var(--white-hsl), 1);
      --tweak-content-link-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-events-item-pagination-date-color: hsla(var(--white-hsl), 1);
      --tweak-events-item-pagination-icon-color: hsla(var(--white-hsl), 1);
      --tweak-events-item-pagination-title-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-form-block-button-background-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-form-block-button-text-color-on-background: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-form-block-caption-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-caption-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-form-block-description-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-description-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-form-block-option-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-option-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-form-block-survey-title-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-survey-title-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-form-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-title-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-gallery-icon-background-color: hsla(var(--darkAccent-hsl), 1);
      --tweak-gallery-icon-color: hsla(var(--white-hsl), 1);
      --tweak-gallery-lightbox-background-color: hsla(var(--darkAccent-hsl), 1);
      --tweak-gallery-lightbox-icon-color: hsla(var(--white-hsl), 1);
      --tweak-heading-extra-large-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-heading-large-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-heading-medium-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-heading-small-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-line-block-line-color: hsla(var(--white-hsl), 1);
      --tweak-marquee-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-marquee-block-heading-color: hsla(var(--white-hsl), 1);
      --tweak-marquee-block-heading-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-marquee-block-paragraph-color: hsla(var(--white-hsl), 1);
      --tweak-marquee-block-paragraph-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-menu-block-item-description-color: hsla(var(--white-hsl), 1);
      --tweak-menu-block-item-price-color: hsla(var(--white-hsl), 1);
      --tweak-menu-block-item-title-color: hsla(var(--white-hsl), 1);
      --tweak-menu-block-nav-color: hsla(var(--white-hsl), 1);
      --tweak-menu-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-newsletter-block-button-background-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-newsletter-block-button-text-color-on-background: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-newsletter-block-description-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-description-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-footnote-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-footnote-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-title-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-paragraph-large-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-paragraph-link-color-on-background: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-paragraph-medium-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-paragraph-small-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-portfolio-item-pagination-icon-color: hsla(var(--white-hsl), 1);
      --tweak-portfolio-item-pagination-meta-color: hsla(var(--white-hsl), 1);
      --tweak-portfolio-item-pagination-title-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-breadcumb-nav-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-description-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-gallery-controls-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-product-basic-item-price-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-scarcity-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-title-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-variant-fields-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-category-nav-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-pagination-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-price-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-scarcity-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-status-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-title-color: hsla(var(--white-hsl), 1);
      --tweak-product-quick-view-button-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-controls-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-overlay-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-source-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-source-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-quote-block-text-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-text-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-excerpt-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-header-text-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-header-text-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-primary-metadata-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-primary-metadata-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-read-more-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-read-more-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-secondary-metadata-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-secondary-metadata-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-title-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-text-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-description-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-meta-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-pagination-icon-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-pagination-title-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-title-color: hsla(var(--white-hsl), 1);
      --video-grid-basic-description-color: hsla(var(--white-hsl), 1);
      --video-grid-basic-meta-color: hsla(var(--white-hsl), 1);
      --video-grid-basic-title-color: hsla(var(--white-hsl), 1);
      --video-grid-category-nav-color: hsla(var(--white-hsl), 1);
    }

    .dark-bold {
      --announcement-bar-background-color: hsla(var(--accent-hsl), 1);
      --announcement-bar-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --backgroundOverlayColor: hsla(var(--darkAccent-hsl), 1);
      --course-item-nav-active-lesson-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --course-item-nav-active-lesson-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --course-item-nav-background-color: hsla(var(--black-hsl), 1);
      --course-item-nav-border-color: hsla(var(--white-hsl), 0.25);
      --course-item-nav-open-tab-background-color: hsla(var(--white-hsl), 0.75);
      --course-item-nav-open-tab-text-color: hsla(var(--black-hsl), 1);
      --course-item-nav-text-color: hsla(var(--white-hsl), 1);
      --course-list-course-item-background: hsla(var(--white-hsl), 1);
      --course-list-course-item-hover-background: hsla(var(--white-hsl), 0.9);
      --course-list-course-item-text-color: hsla(var(--black-hsl), 1);
      --course-list-course-progress-bar-color: hsla(var(--accent-hsl), 1);
      --gradientHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --gradientHeaderBorderColor: hsla(var(--black-hsl), 1);
      --gradientHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --gradientHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --headingExtraLargeColor: hsla(var(--safeLightAccent-hsl), 1);
      --headingLargeColor: hsla(var(--safeLightAccent-hsl), 1);
      --headingLinkColor: hsla(var(--safeLightAccent-hsl), 1);
      --headingMediumColor: hsla(var(--safeLightAccent-hsl), 1);
      --headingSmallColor: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-card-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-card-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-card-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-card-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-card-image-title-bg-color: hsla(var(--darkAccent-hsl), 0);
      --image-block-card-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-card-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-collage-background-color: hsla(var(--white-hsl), 1);
      --image-block-collage-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-collage-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-collage-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-collage-image-subtitle-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-collage-image-title-bg-color: hsla(var(--darkAccent-hsl), 0);
      --image-block-collage-image-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-collage-inline-link-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-overlap-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-overlap-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-overlap-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-overlap-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-overlap-image-title-bg-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-overlap-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-overlap-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-overlay-color: hsla(var(--black-hsl), 0.5);
      --image-block-poster-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-poster-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-poster-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-poster-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-poster-image-title-bg-color-v2: hsla(var(--darkAccent-hsl), 0);
      --image-block-poster-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-poster-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-stack-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-stack-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-stack-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-stack-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-stack-image-title-bg-color: hsla(var(--darkAccent-hsl), 0);
      --image-block-stack-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-stack-inline-link-color: hsla(var(--white-hsl), 1);
      --list-section-banner-slideshow-arrow-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-banner-slideshow-arrow-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-banner-slideshow-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-banner-slideshow-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-color: hsla(var(--white-hsl), 1);
      --list-section-banner-slideshow-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-card-description-link-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-banner-slideshow-card-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-description-color: hsla(var(--white-hsl), 1);
      --list-section-banner-slideshow-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-carousel-arrow-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-carousel-arrow-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-carousel-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-carousel-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-carousel-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-carousel-card-color: hsla(var(--white-hsl), 1);
      --list-section-carousel-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-card-description-link-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-carousel-card-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-description-color: hsla(var(--white-hsl), 1);
      --list-section-carousel-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-simple-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-simple-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-simple-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-simple-card-color: hsla(var(--white-hsl), 1);
      --list-section-simple-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-simple-card-description-link-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-simple-card-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-description-color: hsla(var(--white-hsl), 1);
      --list-section-simple-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --menuOverlayBackgroundColor: hsla(var(--darkAccent-hsl), 1);
      --menuOverlayButtonBackgroundColor: hsla(var(--safeLightAccent-hsl), 1);
      --menuOverlayButtonTextColor: hsla(var(--safeInverseLightAccent-hsl), 1);
      --menuOverlayNavigationLinkColor: hsla(var(--safeLightAccent-hsl), 1);
      --navigationLinkColor: hsla(var(--safeLightAccent-hsl), 1);
      --paragraphLargeColor: hsla(var(--white-hsl), 1);
      --paragraphLinkColor: hsla(var(--safeLightAccent-hsl), 1);
      --paragraphMediumColor: hsla(var(--white-hsl), 1);
      --paragraphSmallColor: hsla(var(--white-hsl), 1);
      --portfolio-grid-basic-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --portfolio-grid-overlay-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --portfolio-grid-overlay-title-color: hsla(var(--white-hsl), 1);
      --portfolio-hover-follow-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --portfolio-hover-static-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --portfolio-index-background-title-color: hsla(var(--white-hsl), 1);
      --primaryButtonBackgroundColor: hsla(var(--safeLightAccent-hsl), 1);
      --primaryButtonTextColor: hsla(var(--safeInverseLightAccent-hsl), 1);
      --secondaryButtonBackgroundColor: hsla(var(--safeLightAccent-hsl), 1);
      --secondaryButtonTextColor: hsla(var(--safeInverseLightAccent-hsl), 1);
      --section-inset-border-color: hsla(var(--white-hsl), 1);
      --shape-block-background-color: hsla(var(--white-hsl), 1);
      --shape-block-dropshadow-color: hsla(var(--white-hsl), 1);
      --siteBackgroundColor: hsla(var(--darkAccent-hsl), 1);
      --siteTitleColor: hsla(var(--safeLightAccent-hsl), 1);
      --social-links-block-main-icon-color: hsla(var(--white-hsl), 1);
      --social-links-block-secondary-icon-color: hsla(var(--darkAccent-hsl), 1);
      --solidHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --solidHeaderBorderColor: hsla(var(--black-hsl), 1);
      --solidHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --solidHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --summary-block-limited-availability-label-color: hsla(var(--white-hsl), 1);
      --tertiaryButtonBackgroundColor: hsla(var(--safeLightAccent-hsl), 1);
      --tertiaryButtonTextColor: hsla(var(--safeInverseLightAccent-hsl), 1);
      --text-highlight-color: hsla(var(--safeLightAccent-hsl), 1);
      --text-highlight-color-on-background: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-accordion-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-accordion-block-divider-color: hsla(var(--white-hsl), 1);
      --tweak-accordion-block-divider-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-accordion-block-icon-color: hsla(var(--white-hsl), 1);
      --tweak-accordion-block-icon-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-basic-grid-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-basic-grid-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-basic-grid-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-basic-grid-list-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-item-author-profile-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-comment-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-comment-text-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-pagination-icon-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-pagination-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-pagination-title-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-masonry-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-masonry-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-masonry-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-masonry-list-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-side-by-side-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-side-by-side-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-side-by-side-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-side-by-side-list-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-single-column-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-single-column-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-single-column-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-single-column-list-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-content-link-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-events-item-pagination-date-color: hsla(var(--white-hsl), 1);
      --tweak-events-item-pagination-icon-color: hsla(var(--white-hsl), 1);
      --tweak-events-item-pagination-title-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-form-block-button-background-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-form-block-button-text-color-on-background: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-form-block-caption-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-caption-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-description-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-description-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-option-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-option-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-survey-title-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-survey-title-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-title-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-gallery-icon-background-color: hsla(var(--darkAccent-hsl), 1);
      --tweak-gallery-icon-color: hsla(var(--white-hsl), 1);
      --tweak-gallery-lightbox-background-color: hsla(var(--darkAccent-hsl), 1);
      --tweak-gallery-lightbox-icon-color: hsla(var(--white-hsl), 1);
      --tweak-heading-extra-large-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-heading-large-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-heading-medium-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-heading-small-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-line-block-line-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-marquee-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-marquee-block-heading-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-marquee-block-heading-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-marquee-block-paragraph-color: hsla(var(--white-hsl), 1);
      --tweak-marquee-block-paragraph-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-menu-block-item-description-color: hsla(var(--white-hsl), 1);
      --tweak-menu-block-item-price-color: hsla(var(--white-hsl), 1);
      --tweak-menu-block-item-title-color: hsla(var(--white-hsl), 1);
      --tweak-menu-block-nav-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-menu-block-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-newsletter-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-newsletter-block-button-background-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-newsletter-block-button-text-color-on-background: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-newsletter-block-description-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-description-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-footnote-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-footnote-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-title-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-paragraph-large-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-paragraph-link-color-on-background: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-paragraph-medium-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-paragraph-small-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-portfolio-item-pagination-icon-color: hsla(var(--white-hsl), 1);
      --tweak-portfolio-item-pagination-meta-color: hsla(var(--white-hsl), 1);
      --tweak-portfolio-item-pagination-title-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-breadcumb-nav-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-description-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-gallery-controls-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-product-basic-item-price-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-scarcity-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-product-basic-item-variant-fields-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-category-nav-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-pagination-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-price-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-scarcity-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-status-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-product-quick-view-button-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-controls-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-overlay-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-source-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-source-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-quote-block-text-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-text-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-excerpt-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-header-text-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-header-text-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-primary-metadata-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-primary-metadata-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-read-more-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-read-more-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-secondary-metadata-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-secondary-metadata-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-title-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-text-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-description-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-meta-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-pagination-icon-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-pagination-title-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-title-color: hsla(var(--accent-hsl), 1);
      --video-grid-basic-description-color: hsla(var(--accent-hsl), 1);
      --video-grid-basic-meta-color: hsla(var(--accent-hsl), 1);
      --video-grid-basic-title-color: hsla(var(--accent-hsl), 1);
      --video-grid-category-nav-color: hsla(var(--accent-hsl), 1);
    }

    .black {
      --announcement-bar-background-color: hsla(var(--white-hsl), 1);
      --announcement-bar-text-color: hsla(var(--black-hsl), 1);
      --backgroundOverlayColor: hsla(var(--black-hsl), 1);
      --course-item-nav-active-lesson-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --course-item-nav-active-lesson-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --course-item-nav-background-color: hsla(var(--darkAccent-hsl), 1);
      --course-item-nav-border-color: hsla(var(--safeInverseDarkAccent-hsl), 0.25);
      --course-item-nav-open-tab-background-color: hsla(var(--white-hsl), 0.75);
      --course-item-nav-open-tab-text-color: hsla(var(--black-hsl), 1);
      --course-item-nav-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --course-list-course-item-background: hsla(var(--white-hsl), 1);
      --course-list-course-item-hover-background: hsla(var(--white-hsl), 0.95);
      --course-list-course-item-text-color: hsla(var(--black-hsl), 1);
      --course-list-course-progress-bar-color: hsla(var(--accent-hsl), 1);
      --gradientHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --gradientHeaderBorderColor: hsla(var(--black-hsl), 1);
      --gradientHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --gradientHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --headingExtraLargeColor: hsla(var(--white-hsl), 1);
      --headingLargeColor: hsla(var(--white-hsl), 1);
      --headingLinkColor: hsla(var(--lightAccent-hsl), 1);
      --headingMediumColor: hsla(var(--white-hsl), 1);
      --headingSmallColor: hsla(var(--white-hsl), 1);
      --image-block-card-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-card-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-card-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-card-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-card-image-title-bg-color: hsla(var(--black-hsl), 0);
      --image-block-card-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-card-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-collage-background-color: hsla(var(--white-hsl), 1);
      --image-block-collage-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-collage-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-collage-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-collage-image-subtitle-color: hsla(var(--black-hsl), 1);
      --image-block-collage-image-title-bg-color: hsla(var(--black-hsl), 0);
      --image-block-collage-image-title-color: hsla(var(--black-hsl), 1);
      --image-block-collage-inline-link-color: hsla(var(--black-hsl), 1);
      --image-block-overlap-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-overlap-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-overlap-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-overlap-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-overlap-image-title-bg-color: hsla(var(--black-hsl), 1);
      --image-block-overlap-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-overlap-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-overlay-color: hsla(var(--black-hsl), 0.5);
      --image-block-poster-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-poster-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-poster-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-poster-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-poster-image-title-bg-color-v2: hsla(var(--black-hsl), 0);
      --image-block-poster-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-poster-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-stack-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-stack-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-stack-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-stack-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-stack-image-title-bg-color: hsla(var(--black-hsl), 0);
      --image-block-stack-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-stack-inline-link-color: hsla(var(--white-hsl), 1);
      --list-section-banner-slideshow-arrow-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-banner-slideshow-arrow-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-banner-slideshow-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-banner-slideshow-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-color: hsla(var(--white-hsl), 1);
      --list-section-banner-slideshow-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-card-description-link-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-banner-slideshow-card-title-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-description-color: hsla(var(--white-hsl), 1);
      --list-section-banner-slideshow-title-color: hsla(var(--white-hsl), 1);
      --list-section-carousel-arrow-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-carousel-arrow-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-carousel-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-carousel-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-carousel-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-carousel-card-color: hsla(var(--white-hsl), 1);
      --list-section-carousel-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-card-description-link-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-carousel-card-title-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-description-color: hsla(var(--white-hsl), 1);
      --list-section-carousel-title-color: hsla(var(--white-hsl), 1);
      --list-section-simple-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-simple-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-simple-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-simple-card-color: hsla(var(--white-hsl), 1);
      --list-section-simple-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-simple-card-description-link-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-simple-card-title-color: hsla(var(--black-hsl), 1);
      --list-section-simple-description-color: hsla(var(--white-hsl), 1);
      --list-section-simple-title-color: hsla(var(--white-hsl), 1);
      --list-section-title-color: hsla(var(--white-hsl), 1);
      --menuOverlayBackgroundColor: hsla(var(--black-hsl), 1);
      --menuOverlayButtonBackgroundColor: hsla(var(--safeLightAccent-hsl), 1);
      --menuOverlayButtonTextColor: hsla(var(--safeInverseLightAccent-hsl), 1);
      --menuOverlayNavigationLinkColor: hsla(var(--white-hsl), 1);
      --navigationLinkColor: hsla(var(--white-hsl), 1);
      --paragraphLargeColor: hsla(var(--white-hsl), 1);
      --paragraphLinkColor: hsla(var(--safeLightAccent-hsl), 1);
      --paragraphMediumColor: hsla(var(--white-hsl), 1);
      --paragraphSmallColor: hsla(var(--white-hsl), 1);
      --portfolio-grid-basic-title-color: hsla(var(--white-hsl), 1);
      --portfolio-grid-overlay-overlay-color: hsla(var(--black-hsl), 1);
      --portfolio-grid-overlay-title-color: hsla(var(--white-hsl), 1);
      --portfolio-hover-follow-title-color: hsla(var(--white-hsl), 1);
      --portfolio-hover-static-title-color: hsla(var(--white-hsl), 1);
      --portfolio-index-background-title-color: hsla(var(--white-hsl), 1);
      --primaryButtonBackgroundColor: hsla(var(--safeLightAccent-hsl), 1);
      --primaryButtonTextColor: hsla(var(--safeInverseLightAccent-hsl), 1);
      --secondaryButtonBackgroundColor: hsla(var(--safeLightAccent-hsl), 1);
      --secondaryButtonTextColor: hsla(var(--safeInverseLightAccent-hsl), 1);
      --section-inset-border-color: hsla(var(--white-hsl), 1);
      --shape-block-background-color: hsla(var(--white-hsl), 1);
      --shape-block-dropshadow-color: hsla(var(--white-hsl), 1);
      --siteBackgroundColor: hsla(var(--black-hsl), 1);
      --siteTitleColor: hsla(var(--white-hsl), 1);
      --social-links-block-main-icon-color: hsla(var(--white-hsl), 1);
      --social-links-block-secondary-icon-color: hsla(var(--black-hsl), 1);
      --solidHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --solidHeaderBorderColor: hsla(var(--black-hsl), 1);
      --solidHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --solidHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --summary-block-limited-availability-label-color: hsla(var(--white-hsl), 1);
      --tertiaryButtonBackgroundColor: hsla(var(--safeLightAccent-hsl), 1);
      --tertiaryButtonTextColor: hsla(var(--safeInverseLightAccent-hsl), 1);
      --text-highlight-color: hsla(var(--safeLightAccent-hsl), 1);
      --text-highlight-color-on-background: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-accordion-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-accordion-block-divider-color: hsla(var(--white-hsl), 1);
      --tweak-accordion-block-divider-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-accordion-block-icon-color: hsla(var(--white-hsl), 1);
      --tweak-accordion-block-icon-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-title-color: hsla(var(--white-hsl), 1);
      --tweak-blog-basic-grid-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-basic-grid-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-basic-grid-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-basic-grid-list-title-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-author-profile-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-comment-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-comment-text-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-pagination-icon-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-pagination-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-pagination-title-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-title-color: hsla(var(--white-hsl), 1);
      --tweak-blog-masonry-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-masonry-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-masonry-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-masonry-list-title-color: hsla(var(--white-hsl), 1);
      --tweak-blog-side-by-side-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-side-by-side-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-side-by-side-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-side-by-side-list-title-color: hsla(var(--white-hsl), 1);
      --tweak-blog-single-column-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-single-column-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-single-column-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-single-column-list-title-color: hsla(var(--white-hsl), 1);
      --tweak-content-link-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-events-item-pagination-date-color: hsla(var(--white-hsl), 1);
      --tweak-events-item-pagination-icon-color: hsla(var(--white-hsl), 1);
      --tweak-events-item-pagination-title-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-form-block-button-background-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-form-block-button-text-color-on-background: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-form-block-caption-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-caption-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-form-block-description-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-description-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-form-block-option-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-option-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-form-block-survey-title-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-survey-title-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-form-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-title-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-gallery-icon-background-color: hsla(var(--black-hsl), 1);
      --tweak-gallery-icon-color: hsla(var(--white-hsl), 1);
      --tweak-gallery-lightbox-background-color: hsla(var(--black-hsl), 1);
      --tweak-gallery-lightbox-icon-color: hsla(var(--white-hsl), 1);
      --tweak-heading-extra-large-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-heading-large-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-heading-medium-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-heading-small-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-line-block-line-color: hsla(var(--white-hsl), 1);
      --tweak-marquee-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-marquee-block-heading-color: hsla(var(--white-hsl), 1);
      --tweak-marquee-block-heading-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-marquee-block-paragraph-color: hsla(var(--white-hsl), 1);
      --tweak-marquee-block-paragraph-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-menu-block-item-description-color: hsla(var(--white-hsl), 1);
      --tweak-menu-block-item-price-color: hsla(var(--white-hsl), 1);
      --tweak-menu-block-item-title-color: hsla(var(--white-hsl), 1);
      --tweak-menu-block-nav-color: hsla(var(--white-hsl), 1);
      --tweak-menu-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-newsletter-block-button-background-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-newsletter-block-button-text-color-on-background: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-newsletter-block-description-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-description-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-footnote-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-footnote-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-newsletter-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-title-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-paragraph-large-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-paragraph-link-color-on-background: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-paragraph-medium-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-paragraph-small-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-portfolio-item-pagination-icon-color: hsla(var(--white-hsl), 1);
      --tweak-portfolio-item-pagination-meta-color: hsla(var(--white-hsl), 1);
      --tweak-portfolio-item-pagination-title-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-breadcumb-nav-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-description-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-gallery-controls-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-product-basic-item-price-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-scarcity-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-title-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-variant-fields-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-category-nav-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-pagination-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-price-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-scarcity-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-status-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-title-color: hsla(var(--white-hsl), 1);
      --tweak-product-quick-view-button-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-controls-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-overlay-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-source-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-source-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-quote-block-text-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-text-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-excerpt-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-header-text-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-header-text-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-primary-metadata-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-primary-metadata-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-read-more-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-read-more-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-secondary-metadata-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-secondary-metadata-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-summary-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-title-color-on-background: hsla(var(--black-hsl), 1);
      --tweak-text-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-description-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-meta-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-pagination-icon-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-pagination-title-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-title-color: hsla(var(--white-hsl), 1);
      --video-grid-basic-description-color: hsla(var(--white-hsl), 1);
      --video-grid-basic-meta-color: hsla(var(--white-hsl), 1);
      --video-grid-basic-title-color: hsla(var(--white-hsl), 1);
      --video-grid-category-nav-color: hsla(var(--white-hsl), 1);
    }

    .black-bold {
      --announcement-bar-background-color: hsla(var(--accent-hsl), 1);
      --announcement-bar-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --backgroundOverlayColor: hsla(var(--black-hsl), 1);
      --course-item-nav-active-lesson-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --course-item-nav-active-lesson-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --course-item-nav-background-color: hsla(var(--black-hsl), 1);
      --course-item-nav-border-color: hsla(var(--white-hsl), 0.25);
      --course-item-nav-open-tab-background-color: hsla(var(--white-hsl), 0.75);
      --course-item-nav-open-tab-text-color: hsla(var(--black-hsl), 1);
      --course-item-nav-text-color: hsla(var(--white-hsl), 1);
      --course-list-course-item-background: hsla(var(--white-hsl), 1);
      --course-list-course-item-hover-background: hsla(var(--white-hsl), 0.95);
      --course-list-course-item-text-color: hsla(var(--black-hsl), 1);
      --course-list-course-progress-bar-color: hsla(var(--accent-hsl), 1);
      --gradientHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --gradientHeaderBorderColor: hsla(var(--black-hsl), 1);
      --gradientHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --gradientHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --headingExtraLargeColor: hsla(var(--safeLightAccent-hsl), 1);
      --headingLargeColor: hsla(var(--safeLightAccent-hsl), 1);
      --headingLinkColor: hsla(var(--safeLightAccent-hsl), 1);
      --headingMediumColor: hsla(var(--safeLightAccent-hsl), 1);
      --headingSmallColor: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-card-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-card-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-card-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-card-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-card-image-title-bg-color: hsla(var(--black-hsl), 0);
      --image-block-card-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-card-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-collage-background-color: hsla(var(--white-hsl), 1);
      --image-block-collage-image-button-bg-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-collage-image-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --image-block-collage-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-collage-image-subtitle-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-collage-image-title-bg-color: hsla(var(--black-hsl), 0);
      --image-block-collage-image-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-collage-inline-link-color: hsla(var(--safeDarkAccent-hsl), 1);
      --image-block-overlap-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-overlap-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-overlap-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-overlap-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-overlap-image-title-bg-color: hsla(var(--black-hsl), 1);
      --image-block-overlap-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-overlap-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-overlay-color: hsla(var(--black-hsl), 0.5);
      --image-block-poster-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-poster-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-poster-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-poster-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-poster-image-title-bg-color-v2: hsla(var(--black-hsl), 0);
      --image-block-poster-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-poster-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-stack-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-stack-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-stack-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-stack-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-stack-image-title-bg-color: hsla(var(--black-hsl), 0);
      --image-block-stack-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-stack-inline-link-color: hsla(var(--white-hsl), 1);
      --list-section-banner-slideshow-arrow-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-banner-slideshow-arrow-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-banner-slideshow-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-banner-slideshow-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-banner-slideshow-card-color: hsla(var(--white-hsl), 1);
      --list-section-banner-slideshow-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-banner-slideshow-card-description-link-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-banner-slideshow-card-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-banner-slideshow-description-color: hsla(var(--white-hsl), 1);
      --list-section-banner-slideshow-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-carousel-arrow-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-carousel-arrow-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-carousel-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-carousel-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-carousel-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-carousel-card-color: hsla(var(--white-hsl), 1);
      --list-section-carousel-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-carousel-card-description-link-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-carousel-card-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-carousel-description-color: hsla(var(--white-hsl), 1);
      --list-section-carousel-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-simple-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-simple-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --list-section-simple-card-button-background-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-card-button-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --list-section-simple-card-color: hsla(var(--white-hsl), 1);
      --list-section-simple-card-description-color: hsla(var(--black-hsl), 1);
      --list-section-simple-card-description-link-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-simple-card-title-color: hsla(var(--safeDarkAccent-hsl), 1);
      --list-section-simple-description-color: hsla(var(--white-hsl), 1);
      --list-section-simple-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --list-section-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --menuOverlayBackgroundColor: hsla(var(--black-hsl), 1);
      --menuOverlayButtonBackgroundColor: hsla(var(--safeLightAccent-hsl), 1);
      --menuOverlayButtonTextColor: hsla(var(--safeInverseLightAccent-hsl), 1);
      --menuOverlayNavigationLinkColor: hsla(var(--safeLightAccent-hsl), 1);
      --navigationLinkColor: hsla(var(--safeLightAccent-hsl), 1);
      --paragraphLargeColor: hsla(var(--white-hsl), 1);
      --paragraphLinkColor: hsla(var(--safeLightAccent-hsl), 1);
      --paragraphMediumColor: hsla(var(--white-hsl), 1);
      --paragraphSmallColor: hsla(var(--white-hsl), 1);
      --portfolio-grid-basic-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --portfolio-grid-overlay-overlay-color: hsla(var(--black-hsl), 1);
      --portfolio-grid-overlay-title-color: hsla(var(--white-hsl), 1);
      --portfolio-hover-follow-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --portfolio-hover-static-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --portfolio-index-background-title-color: hsla(var(--white-hsl), 1);
      --primaryButtonBackgroundColor: hsla(var(--safeLightAccent-hsl), 1);
      --primaryButtonTextColor: hsla(var(--safeInverseLightAccent-hsl), 1);
      --secondaryButtonBackgroundColor: hsla(var(--safeLightAccent-hsl), 1);
      --secondaryButtonTextColor: hsla(var(--safeInverseLightAccent-hsl), 1);
      --section-inset-border-color: hsla(var(--white-hsl), 1);
      --shape-block-background-color: hsla(var(--white-hsl), 1);
      --shape-block-dropshadow-color: hsla(var(--white-hsl), 1);
      --siteBackgroundColor: hsla(var(--black-hsl), 1);
      --siteTitleColor: hsla(var(--safeLightAccent-hsl), 1);
      --social-links-block-main-icon-color: hsla(var(--white-hsl), 1);
      --social-links-block-secondary-icon-color: hsla(var(--black-hsl), 1);
      --solidHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --solidHeaderBorderColor: hsla(var(--black-hsl), 1);
      --solidHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --solidHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --summary-block-limited-availability-label-color: hsla(var(--white-hsl), 1);
      --tertiaryButtonBackgroundColor: hsla(var(--safeLightAccent-hsl), 1);
      --tertiaryButtonTextColor: hsla(var(--safeInverseLightAccent-hsl), 1);
      --text-highlight-color: hsla(var(--safeLightAccent-hsl), 1);
      --text-highlight-color-on-background: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-accordion-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-accordion-block-divider-color: hsla(var(--white-hsl), 1);
      --tweak-accordion-block-divider-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-accordion-block-icon-color: hsla(var(--white-hsl), 1);
      --tweak-accordion-block-icon-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-basic-grid-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-basic-grid-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-basic-grid-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-basic-grid-list-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-item-author-profile-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-comment-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-comment-text-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-pagination-icon-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-pagination-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-pagination-title-color: hsla(var(--white-hsl), 1);
      --tweak-blog-item-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-masonry-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-masonry-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-masonry-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-masonry-list-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-side-by-side-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-side-by-side-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-side-by-side-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-side-by-side-list-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-single-column-list-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-blog-single-column-list-meta-color: hsla(var(--white-hsl), 1);
      --tweak-blog-single-column-list-read-more-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-blog-single-column-list-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-content-link-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-events-item-pagination-date-color: hsla(var(--white-hsl), 1);
      --tweak-events-item-pagination-icon-color: hsla(var(--white-hsl), 1);
      --tweak-events-item-pagination-title-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-form-block-button-background-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-form-block-button-text-color-on-background: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-form-block-caption-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-caption-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-description-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-description-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-option-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-option-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-survey-title-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-survey-title-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-form-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-form-block-title-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-gallery-icon-background-color: hsla(var(--black-hsl), 1);
      --tweak-gallery-icon-color: hsla(var(--white-hsl), 1);
      --tweak-gallery-lightbox-background-color: hsla(var(--black-hsl), 1);
      --tweak-gallery-lightbox-icon-color: hsla(var(--white-hsl), 1);
      --tweak-heading-extra-large-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-heading-large-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-heading-medium-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-heading-small-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-line-block-line-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-marquee-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-marquee-block-heading-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-marquee-block-heading-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-marquee-block-paragraph-color: hsla(var(--white-hsl), 1);
      --tweak-marquee-block-paragraph-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-menu-block-item-description-color: hsla(var(--white-hsl), 1);
      --tweak-menu-block-item-price-color: hsla(var(--white-hsl), 1);
      --tweak-menu-block-item-title-color: hsla(var(--white-hsl), 1);
      --tweak-menu-block-nav-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-menu-block-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-newsletter-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-button-background-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-newsletter-block-button-background-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-newsletter-block-button-text-color-on-background: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --tweak-newsletter-block-description-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-description-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-footnote-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-footnote-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-newsletter-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-newsletter-block-title-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-paragraph-large-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-paragraph-link-color-on-background: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-paragraph-medium-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-paragraph-small-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-portfolio-item-pagination-icon-color: hsla(var(--white-hsl), 1);
      --tweak-portfolio-item-pagination-meta-color: hsla(var(--white-hsl), 1);
      --tweak-portfolio-item-pagination-title-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-breadcumb-nav-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-description-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-gallery-controls-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-product-basic-item-price-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-scarcity-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-product-basic-item-variant-fields-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-category-nav-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-pagination-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-price-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-scarcity-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-status-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-title-color: hsla(var(--safeLightAccent-hsl), 1);
      --tweak-product-quick-view-button-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-controls-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-overlay-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-source-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-source-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-quote-block-text-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-text-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-excerpt-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-excerpt-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-header-text-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-header-text-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-primary-metadata-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-primary-metadata-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-read-more-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-read-more-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-secondary-metadata-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-secondary-metadata-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-summary-block-title-color: hsla(var(--white-hsl), 1);
      --tweak-summary-block-title-color-on-background: hsla(var(--safeDarkAccent-hsl), 1);
      --tweak-text-block-background-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-description-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-meta-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-pagination-icon-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-pagination-title-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-title-color: hsla(var(--accent-hsl), 1);
      --video-grid-basic-description-color: hsla(var(--accent-hsl), 1);
      --video-grid-basic-meta-color: hsla(var(--accent-hsl), 1);
      --video-grid-basic-title-color: hsla(var(--accent-hsl), 1);
      --video-grid-category-nav-color: hsla(var(--accent-hsl), 1);
    }

    .bright {
      --announcement-bar-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --announcement-bar-text-color: hsla(var(--accent-hsl), 1);
      --backgroundOverlayColor: hsla(var(--accent-hsl), 1);
      --course-item-nav-active-lesson-background-color: hsla(var(--lightAccent-hsl), 1);
      --course-item-nav-active-lesson-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --course-item-nav-background-color: hsla(var(--black-hsl), 1);
      --course-item-nav-border-color: hsla(var(--white-hsl), 0.25);
      --course-item-nav-open-tab-background-color: hsla(var(--black-hsl), 0.75);
      --course-item-nav-open-tab-text-color: hsla(var(--white-hsl), 1);
      --course-item-nav-text-color: hsla(var(--white-hsl), 1);
      --course-list-course-item-background: hsla(var(--safeInverseAccent-hsl), 1);
      --course-list-course-item-hover-background: hsla(var(--safeInverseAccent-hsl), 0.9);
      --course-list-course-item-text-color: hsla(var(--accent-hsl), 1);
      --course-list-course-progress-bar-color: hsla(var(--darkAccent-hsl), 1);
      --gradientHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --gradientHeaderBorderColor: hsla(var(--black-hsl), 1);
      --gradientHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --gradientHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --headingExtraLargeColor: hsla(var(--safeInverseAccent-hsl), 1);
      --headingLargeColor: hsla(var(--safeInverseAccent-hsl), 1);
      --headingLinkColor: hsla(var(--safeInverseAccent-hsl), 1);
      --headingMediumColor: hsla(var(--safeInverseAccent-hsl), 1);
      --headingSmallColor: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-card-image-button-bg-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-card-image-button-text-color: hsla(var(--accent-hsl), 1);
      --image-block-card-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-card-image-subtitle-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-card-image-title-bg-color: hsla(var(--accent-hsl), 0);
      --image-block-card-image-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-card-inline-link-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-collage-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-collage-image-button-bg-color: hsla(var(--accent-hsl), 1);
      --image-block-collage-image-button-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-collage-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-collage-image-subtitle-color: hsla(var(--accent-hsl), 1);
      --image-block-collage-image-title-bg-color: hsla(var(--accent-hsl), 0);
      --image-block-collage-image-title-color: hsla(var(--accent-hsl), 1);
      --image-block-collage-inline-link-color: hsla(var(--accent-hsl), 1);
      --image-block-overlap-image-button-bg-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-overlap-image-button-text-color: hsla(var(--accent-hsl), 1);
      --image-block-overlap-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-overlap-image-subtitle-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-overlap-image-title-bg-color: hsla(var(--accent-hsl), 1);
      --image-block-overlap-image-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-overlap-inline-link-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-overlay-color: hsla(var(--black-hsl), 0.5);
      --image-block-poster-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-poster-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-poster-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-poster-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-poster-image-title-bg-color-v2: hsla(var(--accent-hsl), 0);
      --image-block-poster-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-poster-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-stack-image-button-bg-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-stack-image-button-text-color: hsla(var(--accent-hsl), 1);
      --image-block-stack-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-stack-image-subtitle-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-stack-image-title-bg-color: hsla(var(--accent-hsl), 0);
      --image-block-stack-image-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-stack-inline-link-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-banner-slideshow-arrow-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-banner-slideshow-arrow-color: hsla(var(--accent-hsl), 1);
      --list-section-banner-slideshow-button-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-banner-slideshow-button-text-color: hsla(var(--accent-hsl), 1);
      --list-section-banner-slideshow-card-button-background-color: hsla(var(--accent-hsl), 1);
      --list-section-banner-slideshow-card-button-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-banner-slideshow-card-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-banner-slideshow-card-description-color: hsla(var(--accent-hsl), 1);
      --list-section-banner-slideshow-card-description-link-color: hsla(var(--accent-hsl), 1);
      --list-section-banner-slideshow-card-title-color: hsla(var(--accent-hsl), 1);
      --list-section-banner-slideshow-description-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-banner-slideshow-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-carousel-arrow-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-carousel-arrow-color: hsla(var(--accent-hsl), 1);
      --list-section-carousel-button-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-carousel-button-text-color: hsla(var(--accent-hsl), 1);
      --list-section-carousel-card-button-background-color: hsla(var(--accent-hsl), 1);
      --list-section-carousel-card-button-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-carousel-card-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-carousel-card-description-color: hsla(var(--accent-hsl), 1);
      --list-section-carousel-card-description-link-color: hsla(var(--accent-hsl), 1);
      --list-section-carousel-card-title-color: hsla(var(--accent-hsl), 1);
      --list-section-carousel-description-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-carousel-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-simple-button-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-simple-button-text-color: hsla(var(--accent-hsl), 1);
      --list-section-simple-card-button-background-color: hsla(var(--accent-hsl), 1);
      --list-section-simple-card-button-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-simple-card-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-simple-card-description-color: hsla(var(--accent-hsl), 1);
      --list-section-simple-card-description-link-color: hsla(var(--accent-hsl), 1);
      --list-section-simple-card-title-color: hsla(var(--accent-hsl), 1);
      --list-section-simple-description-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-simple-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --menuOverlayBackgroundColor: hsla(var(--accent-hsl), 1);
      --menuOverlayButtonBackgroundColor: hsla(var(--safeInverseAccent-hsl), 1);
      --menuOverlayButtonTextColor: hsla(var(--accent-hsl), 1);
      --menuOverlayNavigationLinkColor: hsla(var(--safeInverseAccent-hsl), 1);
      --navigationLinkColor: hsla(var(--safeInverseAccent-hsl), 1);
      --paragraphLargeColor: hsla(var(--safeInverseAccent-hsl), 1);
      --paragraphLinkColor: hsla(var(--safeInverseAccent-hsl), 1);
      --paragraphMediumColor: hsla(var(--safeInverseAccent-hsl), 1);
      --paragraphSmallColor: hsla(var(--safeInverseAccent-hsl), 1);
      --portfolio-grid-basic-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --portfolio-grid-overlay-overlay-color: hsla(var(--accent-hsl), 1);
      --portfolio-grid-overlay-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --portfolio-hover-follow-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --portfolio-hover-static-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --portfolio-index-background-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --primaryButtonBackgroundColor: hsla(var(--safeInverseAccent-hsl), 1);
      --primaryButtonTextColor: hsla(var(--accent-hsl), 1);
      --secondaryButtonBackgroundColor: hsla(var(--safeInverseAccent-hsl), 1);
      --secondaryButtonTextColor: hsla(var(--accent-hsl), 1);
      --section-inset-border-color: hsla(var(--white-hsl), 1);
      --shape-block-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --shape-block-dropshadow-color: hsla(var(--safeInverseAccent-hsl), 1);
      --siteBackgroundColor: hsla(var(--accent-hsl), 1);
      --siteTitleColor: hsla(var(--safeInverseAccent-hsl), 1);
      --social-links-block-main-icon-color: hsla(var(--safeInverseAccent-hsl), 1);
      --social-links-block-secondary-icon-color: hsla(var(--accent-hsl), 1);
      --solidHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --solidHeaderBorderColor: hsla(var(--black-hsl), 1);
      --solidHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --solidHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --summary-block-limited-availability-label-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tertiaryButtonBackgroundColor: hsla(var(--safeInverseAccent-hsl), 1);
      --tertiaryButtonTextColor: hsla(var(--accent-hsl), 1);
      --text-highlight-color: hsla(var(--safeInverseAccent-hsl), 1);
      --text-highlight-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-accordion-block-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-accordion-block-divider-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-accordion-block-divider-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-accordion-block-icon-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-accordion-block-icon-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-excerpt-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-meta-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-read-more-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-basic-grid-list-excerpt-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-basic-grid-list-meta-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-basic-grid-list-read-more-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-basic-grid-list-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-item-author-profile-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-item-comment-meta-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-item-comment-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-item-meta-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-item-pagination-icon-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-item-pagination-meta-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-item-pagination-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-item-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-masonry-list-excerpt-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-masonry-list-meta-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-masonry-list-read-more-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-masonry-list-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-side-by-side-list-excerpt-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-side-by-side-list-meta-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-side-by-side-list-read-more-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-side-by-side-list-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-single-column-list-excerpt-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-single-column-list-meta-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-single-column-list-read-more-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-single-column-list-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-content-link-block-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-events-item-pagination-date-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-events-item-pagination-icon-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-events-item-pagination-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-form-block-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-form-block-button-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-form-block-button-background-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-form-block-button-text-color: hsla(var(--accent-hsl), 1);
      --tweak-form-block-button-text-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-form-block-caption-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-form-block-caption-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-form-block-description-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-form-block-description-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-form-block-option-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-form-block-option-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-form-block-survey-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-form-block-survey-title-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-form-block-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-form-block-title-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-gallery-icon-background-color: hsla(var(--accent-hsl), 1);
      --tweak-gallery-icon-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-gallery-lightbox-background-color: hsla(var(--accent-hsl), 1);
      --tweak-gallery-lightbox-icon-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-heading-extra-large-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-heading-large-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-heading-medium-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-heading-small-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-line-block-line-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-marquee-block-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-marquee-block-heading-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-marquee-block-heading-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-marquee-block-paragraph-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-marquee-block-paragraph-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-menu-block-item-description-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-menu-block-item-price-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-menu-block-item-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-menu-block-nav-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-menu-block-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-newsletter-block-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-newsletter-block-button-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-newsletter-block-button-background-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-newsletter-block-button-text-color: hsla(var(--accent-hsl), 1);
      --tweak-newsletter-block-button-text-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-newsletter-block-description-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-newsletter-block-description-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-newsletter-block-footnote-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-newsletter-block-footnote-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-newsletter-block-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-newsletter-block-title-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-paragraph-large-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-paragraph-link-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-paragraph-medium-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-paragraph-small-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-portfolio-item-pagination-icon-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-portfolio-item-pagination-meta-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-portfolio-item-pagination-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-product-basic-item-breadcumb-nav-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-product-basic-item-description-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-product-basic-item-gallery-controls-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-product-basic-item-price-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-product-basic-item-sale-price-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-product-basic-item-scarcity-color: hsla(var(--white-hsl), 1);
      --tweak-product-basic-item-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-product-basic-item-variant-fields-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-product-grid-text-below-list-category-nav-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-product-grid-text-below-list-pagination-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-product-grid-text-below-list-price-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-product-grid-text-below-list-sale-price-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-scarcity-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-product-grid-text-below-list-status-color: hsla(var(--white-hsl), 1);
      --tweak-product-grid-text-below-list-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-product-quick-view-button-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-controls-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-overlay-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-quote-block-source-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-quote-block-source-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-quote-block-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-quote-block-text-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-summary-block-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-summary-block-excerpt-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-summary-block-excerpt-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-summary-block-header-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-summary-block-header-text-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-summary-block-primary-metadata-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-summary-block-primary-metadata-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-summary-block-read-more-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-summary-block-read-more-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-summary-block-secondary-metadata-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-summary-block-secondary-metadata-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-summary-block-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-summary-block-title-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-text-block-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-video-item-description-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-meta-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-pagination-icon-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-pagination-title-color: hsla(var(--white-hsl), 1);
      --tweak-video-item-title-color: hsla(var(--white-hsl), 1);
      --video-grid-basic-description-color: hsla(var(--white-hsl), 1);
      --video-grid-basic-meta-color: hsla(var(--white-hsl), 1);
      --video-grid-basic-title-color: hsla(var(--white-hsl), 1);
      --video-grid-category-nav-color: hsla(var(--white-hsl), 1);
    }

    .bright-inverse {
      --announcement-bar-background-color: hsla(var(--accent-hsl), 1);
      --announcement-bar-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --backgroundOverlayColor: hsla(var(--safeInverseAccent-hsl), 1);
      --course-item-nav-active-lesson-background-color: hsla(var(--darkAccent-hsl), 1);
      --course-item-nav-active-lesson-text-color: hsla(var(--safeInverseDarkAccent-hsl), 1);
      --course-item-nav-background-color: hsla(var(--accent-hsl), 1);
      --course-item-nav-border-color: hsla(var(--safeInverseAccent-hsl), 0.25);
      --course-item-nav-open-tab-background-color: hsla(var(--black-hsl), 0.75);
      --course-item-nav-open-tab-text-color: hsla(var(--white-hsl), 1);
      --course-item-nav-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --course-list-course-item-background: hsla(var(--accent-hsl), 1);
      --course-list-course-item-hover-background: hsla(var(--accent-hsl), 0.95);
      --course-list-course-item-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --course-list-course-progress-bar-color: hsla(var(--darkAccent-hsl), 1);
      --gradientHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --gradientHeaderBorderColor: hsla(var(--black-hsl), 1);
      --gradientHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --gradientHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --headingExtraLargeColor: hsla(var(--accent-hsl), 1);
      --headingLargeColor: hsla(var(--accent-hsl), 1);
      --headingLinkColor: hsla(var(--accent-hsl), 1);
      --headingMediumColor: hsla(var(--accent-hsl), 1);
      --headingSmallColor: hsla(var(--accent-hsl), 1);
      --image-block-card-image-button-bg-color: hsla(var(--accent-hsl), 1);
      --image-block-card-image-button-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-card-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-card-image-subtitle-color: hsla(var(--accent-hsl), 1);
      --image-block-card-image-title-bg-color: hsla(var(--safeInverseAccent-hsl), 0);
      --image-block-card-image-title-color: hsla(var(--accent-hsl), 1);
      --image-block-card-inline-link-color: hsla(var(--accent-hsl), 1);
      --image-block-collage-background-color: hsla(var(--accent-hsl), 1);
      --image-block-collage-image-button-bg-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-collage-image-button-text-color: hsla(var(--accent-hsl), 1);
      --image-block-collage-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-collage-image-subtitle-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-collage-image-title-bg-color: hsla(var(--safeInverseAccent-hsl), 0);
      --image-block-collage-image-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-collage-inline-link-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-overlap-image-button-bg-color: hsla(var(--accent-hsl), 1);
      --image-block-overlap-image-button-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-overlap-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-overlap-image-subtitle-color: hsla(var(--accent-hsl), 1);
      --image-block-overlap-image-title-bg-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-overlap-image-title-color: hsla(var(--accent-hsl), 1);
      --image-block-overlap-inline-link-color: hsla(var(--accent-hsl), 1);
      --image-block-overlay-color: hsla(var(--black-hsl), 0.5);
      --image-block-poster-image-button-bg-color: hsla(var(--safeLightAccent-hsl), 1);
      --image-block-poster-image-button-text-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --image-block-poster-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-poster-image-subtitle-color: hsla(var(--white-hsl), 1);
      --image-block-poster-image-title-bg-color-v2: hsla(var(--safeInverseAccent-hsl), 0);
      --image-block-poster-image-title-color: hsla(var(--white-hsl), 1);
      --image-block-poster-inline-link-color: hsla(var(--white-hsl), 1);
      --image-block-stack-image-button-bg-color: hsla(var(--accent-hsl), 1);
      --image-block-stack-image-button-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --image-block-stack-image-overlay-color: hsla(var(--darkAccent-hsl), 1);
      --image-block-stack-image-subtitle-color: hsla(var(--accent-hsl), 1);
      --image-block-stack-image-title-bg-color: hsla(var(--safeInverseAccent-hsl), 0);
      --image-block-stack-image-title-color: hsla(var(--accent-hsl), 1);
      --image-block-stack-inline-link-color: hsla(var(--accent-hsl), 1);
      --list-section-banner-slideshow-arrow-background-color: hsla(var(--accent-hsl), 1);
      --list-section-banner-slideshow-arrow-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-banner-slideshow-button-background-color: hsla(var(--accent-hsl), 1);
      --list-section-banner-slideshow-button-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-banner-slideshow-card-button-text-color: hsla(var(--accent-hsl), 1);
      --list-section-banner-slideshow-card-color: hsla(var(--accent-hsl), 1);
      --list-section-banner-slideshow-card-description-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-banner-slideshow-card-description-link-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-banner-slideshow-card-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-banner-slideshow-description-color: hsla(var(--accent-hsl), 1);
      --list-section-banner-slideshow-title-color: hsla(var(--accent-hsl), 1);
      --list-section-carousel-arrow-background-color: hsla(var(--accent-hsl), 1);
      --list-section-carousel-arrow-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-carousel-button-background-color: hsla(var(--accent-hsl), 1);
      --list-section-carousel-button-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-carousel-card-button-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-carousel-card-button-text-color: hsla(var(--accent-hsl), 1);
      --list-section-carousel-card-color: hsla(var(--accent-hsl), 1);
      --list-section-carousel-card-description-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-carousel-card-description-link-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-carousel-card-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-carousel-description-color: hsla(var(--accent-hsl), 1);
      --list-section-carousel-title-color: hsla(var(--accent-hsl), 1);
      --list-section-simple-button-background-color: hsla(var(--accent-hsl), 1);
      --list-section-simple-button-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-simple-card-button-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-simple-card-button-text-color: hsla(var(--accent-hsl), 1);
      --list-section-simple-card-color: hsla(var(--accent-hsl), 1);
      --list-section-simple-card-description-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-simple-card-description-link-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-simple-card-title-color: hsla(var(--safeInverseAccent-hsl), 1);
      --list-section-simple-description-color: hsla(var(--accent-hsl), 1);
      --list-section-simple-title-color: hsla(var(--accent-hsl), 1);
      --list-section-title-color: hsla(var(--accent-hsl), 1);
      --menuOverlayBackgroundColor: hsla(var(--safeInverseAccent-hsl), 1);
      --menuOverlayButtonBackgroundColor: hsla(var(--accent-hsl), 1);
      --menuOverlayButtonTextColor: hsla(var(--safeInverseAccent-hsl), 1);
      --menuOverlayNavigationLinkColor: hsla(var(--accent-hsl), 1);
      --navigationLinkColor: hsla(var(--accent-hsl), 1);
      --paragraphLargeColor: hsla(var(--accent-hsl), 1);
      --paragraphLinkColor: hsla(var(--accent-hsl), 1);
      --paragraphMediumColor: hsla(var(--accent-hsl), 1);
      --paragraphSmallColor: hsla(var(--accent-hsl), 1);
      --portfolio-grid-basic-title-color: hsla(var(--accent-hsl), 1);
      --portfolio-grid-overlay-overlay-color: hsla(var(--safeInverseAccent-hsl), 1);
      --portfolio-grid-overlay-title-color: hsla(var(--accent-hsl), 1);
      --portfolio-hover-follow-title-color: hsla(var(--accent-hsl), 1);
      --portfolio-hover-static-title-color: hsla(var(--accent-hsl), 1);
      --portfolio-index-background-title-color: hsla(var(--accent-hsl), 1);
      --primaryButtonBackgroundColor: hsla(var(--accent-hsl), 1);
      --primaryButtonTextColor: hsla(var(--safeInverseAccent-hsl), 1);
      --secondaryButtonBackgroundColor: hsla(var(--accent-hsl), 1);
      --secondaryButtonTextColor: hsla(var(--safeInverseAccent-hsl), 1);
      --section-inset-border-color: hsla(var(--white-hsl), 1);
      --shape-block-background-color: hsla(var(--accent-hsl), 1);
      --shape-block-dropshadow-color: hsla(var(--accent-hsl), 1);
      --siteBackgroundColor: hsla(var(--safeInverseAccent-hsl), 1);
      --siteTitleColor: hsla(var(--accent-hsl), 1);
      --social-links-block-main-icon-color: hsla(var(--accent-hsl), 1);
      --social-links-block-secondary-icon-color: hsla(var(--safeInverseAccent-hsl), 1);
      --solidHeaderBackgroundColor: hsla(var(--white-hsl), 1);
      --solidHeaderBorderColor: hsla(var(--black-hsl), 1);
      --solidHeaderDropShadowColor: hsla(var(--black-hsl), 1);
      --solidHeaderNavigationColor: hsla(var(--black-hsl), 1);
      --summary-block-limited-availability-label-color: hsla(var(--accent-hsl), 1);
      --tertiaryButtonBackgroundColor: hsla(var(--accent-hsl), 1);
      --tertiaryButtonTextColor: hsla(var(--safeInverseAccent-hsl), 1);
      --text-highlight-color: hsla(var(--accent-hsl), 1);
      --text-highlight-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-accordion-block-background-color: hsla(var(--accent-hsl), 1);
      --tweak-accordion-block-divider-color: hsla(var(--accent-hsl), 1);
      --tweak-accordion-block-divider-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-accordion-block-icon-color: hsla(var(--accent-hsl), 1);
      --tweak-accordion-block-icon-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-excerpt-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-meta-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-read-more-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-alternating-side-by-side-list-title-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-basic-grid-list-excerpt-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-basic-grid-list-meta-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-basic-grid-list-read-more-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-basic-grid-list-title-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-item-author-profile-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-item-comment-meta-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-item-comment-text-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-item-meta-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-item-pagination-icon-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-item-pagination-meta-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-item-pagination-title-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-item-title-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-masonry-list-excerpt-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-masonry-list-meta-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-masonry-list-read-more-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-masonry-list-title-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-side-by-side-list-excerpt-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-side-by-side-list-meta-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-side-by-side-list-read-more-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-side-by-side-list-title-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-single-column-list-excerpt-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-single-column-list-meta-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-single-column-list-read-more-color: hsla(var(--accent-hsl), 1);
      --tweak-blog-single-column-list-title-color: hsla(var(--accent-hsl), 1);
      --tweak-content-link-block-title-color: hsla(var(--accent-hsl), 1);
      --tweak-events-item-pagination-date-color: hsla(var(--accent-hsl), 1);
      --tweak-events-item-pagination-icon-color: hsla(var(--accent-hsl), 1);
      --tweak-events-item-pagination-title-color: hsla(var(--accent-hsl), 1);
      --tweak-form-block-background-color: hsla(var(--accent-hsl), 1);
      --tweak-form-block-button-background-color: hsla(var(--accent-hsl), 1);
      --tweak-form-block-button-background-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-form-block-button-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-form-block-button-text-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-form-block-caption-color: hsla(var(--accent-hsl), 1);
      --tweak-form-block-caption-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-form-block-description-color: hsla(var(--accent-hsl), 1);
      --tweak-form-block-description-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-form-block-option-color: hsla(var(--accent-hsl), 1);
      --tweak-form-block-option-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-form-block-survey-title-color: hsla(var(--accent-hsl), 1);
      --tweak-form-block-survey-title-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-form-block-title-color: hsla(var(--accent-hsl), 1);
      --tweak-form-block-title-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-gallery-icon-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-gallery-icon-color: hsla(var(--accent-hsl), 1);
      --tweak-gallery-lightbox-background-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-gallery-lightbox-icon-color: hsla(var(--accent-hsl), 1);
      --tweak-heading-extra-large-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-heading-large-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-heading-medium-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-heading-small-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-line-block-line-color: hsla(var(--accent-hsl), 1);
      --tweak-marquee-block-background-color: hsla(var(--accent-hsl), 1);
      --tweak-marquee-block-heading-color: hsla(var(--accent-hsl), 1);
      --tweak-marquee-block-heading-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-marquee-block-paragraph-color: hsla(var(--accent-hsl), 1);
      --tweak-marquee-block-paragraph-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-menu-block-item-description-color: hsla(var(--accent-hsl), 1);
      --tweak-menu-block-item-price-color: hsla(var(--accent-hsl), 1);
      --tweak-menu-block-item-title-color: hsla(var(--accent-hsl), 1);
      --tweak-menu-block-nav-color: hsla(var(--accent-hsl), 1);
      --tweak-menu-block-title-color: hsla(var(--accent-hsl), 1);
      --tweak-newsletter-block-background-color: hsla(var(--accent-hsl), 1);
      --tweak-newsletter-block-button-background-color: hsla(var(--accent-hsl), 1);
      --tweak-newsletter-block-button-background-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-newsletter-block-button-text-color: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-newsletter-block-button-text-color-on-background: hsla(var(--accent-hsl), 1);
      --tweak-newsletter-block-description-color: hsla(var(--accent-hsl), 1);
      --tweak-newsletter-block-description-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-newsletter-block-footnote-color: hsla(var(--accent-hsl), 1);
      --tweak-newsletter-block-footnote-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-newsletter-block-title-color: hsla(var(--accent-hsl), 1);
      --tweak-newsletter-block-title-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-paragraph-large-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-paragraph-link-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-paragraph-medium-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-paragraph-small-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-portfolio-item-pagination-icon-color: hsla(var(--accent-hsl), 1);
      --tweak-portfolio-item-pagination-meta-color: hsla(var(--accent-hsl), 1);
      --tweak-portfolio-item-pagination-title-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-breadcumb-nav-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-description-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-gallery-controls-color: hsla(var(--safeInverseLightAccent-hsl), 1);
      --tweak-product-basic-item-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-scarcity-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-title-color: hsla(var(--accent-hsl), 1);
      --tweak-product-basic-item-variant-fields-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-category-nav-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-pagination-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-sale-price-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-scarcity-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-status-color: hsla(var(--accent-hsl), 1);
      --tweak-product-grid-text-below-list-title-color: hsla(var(--accent-hsl), 1);
      --tweak-product-quick-view-button-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-controls-color: hsla(var(--black-hsl), 1);
      --tweak-product-quick-view-lightbox-overlay-color: hsla(var(--white-hsl), 1);
      --tweak-quote-block-background-color: hsla(var(--accent-hsl), 1);
      --tweak-quote-block-source-color: hsla(var(--accent-hsl), 1);
      --tweak-quote-block-source-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-quote-block-text-color: hsla(var(--accent-hsl), 1);
      --tweak-quote-block-text-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-summary-block-background-color: hsla(var(--accent-hsl), 1);
      --tweak-summary-block-excerpt-color: hsla(var(--accent-hsl), 1);
      --tweak-summary-block-excerpt-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-summary-block-header-text-color: hsla(var(--accent-hsl), 1);
      --tweak-summary-block-header-text-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-summary-block-primary-metadata-color: hsla(var(--accent-hsl), 1);
      --tweak-summary-block-primary-metadata-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-summary-block-read-more-color: hsla(var(--accent-hsl), 1);
      --tweak-summary-block-read-more-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-summary-block-secondary-metadata-color: hsla(var(--accent-hsl), 1);
      --tweak-summary-block-secondary-metadata-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-summary-block-title-color: hsla(var(--accent-hsl), 1);
      --tweak-summary-block-title-color-on-background: hsla(var(--safeInverseAccent-hsl), 1);
      --tweak-text-block-background-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-description-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-meta-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-pagination-icon-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-pagination-title-color: hsla(var(--accent-hsl), 1);
      --tweak-video-item-title-color: hsla(var(--accent-hsl), 1);
      --video-grid-basic-description-color: hsla(var(--accent-hsl), 1);
      --video-grid-basic-meta-color: hsla(var(--accent-hsl), 1);
      --video-grid-basic-title-color: hsla(var(--accent-hsl), 1);
      --video-grid-category-nav-color: hsla(var(--accent-hsl), 1);
    }
  </style>


  <style id="rteTextColorMapping">
    .sqsrte-text-color--white {
      color: hsla(var(--white-hsl), 1);
    }

    .sqsrte-text-color--black {
      color: hsla(var(--black-hsl), 1);
    }

    .sqsrte-text-color--safeLightAccent {
      color: hsla(var(--safeLightAccent-hsl), 1);
    }

    .sqsrte-text-color--safeDarkAccent {
      color: hsla(var(--safeDarkAccent-hsl), 1);
    }

    .sqsrte-text-color--safeInverseAccent {
      color: hsla(var(--safeInverseAccent-hsl), 1);
    }

    .sqsrte-text-color--safeInverseLightAccent {
      color: hsla(var(--safeInverseLightAccent-hsl), 1);
    }

    .sqsrte-text-color--safeInverseDarkAccent {
      color: hsla(var(--safeInverseDarkAccent-hsl), 1);
    }

    .sqsrte-text-color--accent {
      color: hsla(var(--accent-hsl), 1);
    }

    .sqsrte-text-color--lightAccent {
      color: hsla(var(--lightAccent-hsl), 1);
    }

    .sqsrte-text-color--darkAccent {
      color: hsla(var(--darkAccent-hsl), 1);
    }
  </style>
</head>

<body id="collection-6298ea29a2382b218be83236" class="
      header-overlay-alignment-center header-width-full   tweak-fixed-header-style-scroll-back tweak-blog-alternating-side-by-side-width-inset tweak-blog-alternating-side-by-side-image-aspect-ratio-11-square tweak-blog-alternating-side-by-side-text-alignment-left tweak-blog-alternating-side-by-side-read-more-style-show tweak-blog-alternating-side-by-side-image-text-alignment-middle tweak-blog-alternating-side-by-side-delimiter-dash tweak-blog-alternating-side-by-side-meta-position-top tweak-blog-alternating-side-by-side-primary-meta-categories tweak-blog-alternating-side-by-side-secondary-meta-date tweak-blog-alternating-side-by-side-excerpt-show tweak-blog-basic-grid-width-inset tweak-blog-basic-grid-image-aspect-ratio-169-widescreen tweak-blog-basic-grid-text-alignment-center tweak-blog-basic-grid-delimiter-bullet tweak-blog-basic-grid-image-placement-above tweak-blog-basic-grid-read-more-style-show tweak-blog-basic-grid-primary-meta-categories tweak-blog-basic-grid-secondary-meta-date tweak-blog-basic-grid-excerpt-show tweak-blog-item-width-narrow tweak-blog-item-text-alignment-left tweak-blog-item-meta-position-above-title tweak-blog-item-show-categories tweak-blog-item-show-date tweak-blog-item-show-author-name tweak-blog-item-show-author-profile tweak-blog-item-delimiter-dash tweak-blog-masonry-width-full tweak-blog-masonry-text-alignment-center tweak-blog-masonry-primary-meta-categories tweak-blog-masonry-secondary-meta-date tweak-blog-masonry-meta-position-top tweak-blog-masonry-read-more-style-show tweak-blog-masonry-delimiter-space tweak-blog-masonry-image-placement-above tweak-blog-masonry-excerpt-show tweak-blog-side-by-side-width-inset tweak-blog-side-by-side-image-placement-left tweak-blog-side-by-side-image-aspect-ratio-11-square tweak-blog-side-by-side-primary-meta-categories tweak-blog-side-by-side-secondary-meta-date tweak-blog-side-by-side-meta-position-top tweak-blog-side-by-side-text-alignment-left tweak-blog-side-by-side-image-text-alignment-middle tweak-blog-side-by-side-read-more-style-show tweak-blog-side-by-side-delimiter-bullet tweak-blog-side-by-side-excerpt-show tweak-blog-single-column-width-inset tweak-blog-single-column-text-alignment-center tweak-blog-single-column-image-placement-above tweak-blog-single-column-delimiter-bullet tweak-blog-single-column-read-more-style-hide tweak-blog-single-column-primary-meta-categories tweak-blog-single-column-secondary-meta-date tweak-blog-single-column-meta-position-top tweak-blog-single-column-content-full-post tweak-events-stacked-width-full tweak-events-stacked-height-small tweak-events-stacked-show-past-events tweak-events-stacked-show-thumbnails tweak-events-stacked-thumbnail-size-32-standard tweak-events-stacked-date-style-side-tag tweak-events-stacked-show-time tweak-events-stacked-show-location tweak-events-stacked-ical-gcal-links tweak-events-stacked-show-excerpt   tweak-global-animations-complexity-level-detailed tweak-global-animations-animation-style-fade tweak-global-animations-animation-type-none tweak-global-animations-animation-curve-ease tweak-portfolio-grid-basic-width-inset tweak-portfolio-grid-basic-height-medium tweak-portfolio-grid-basic-image-aspect-ratio-43-four-three tweak-portfolio-grid-basic-text-alignment-left tweak-portfolio-grid-basic-hover-effect-zoom tweak-portfolio-grid-overlay-width-full tweak-portfolio-grid-overlay-height-small tweak-portfolio-grid-overlay-image-aspect-ratio-43-four-three tweak-portfolio-grid-overlay-text-placement-center tweak-portfolio-grid-overlay-show-text-after-hover tweak-portfolio-index-background-link-format-stacked tweak-portfolio-index-background-width-full-bleed tweak-portfolio-index-background-height-large  tweak-portfolio-index-background-vertical-alignment-middle tweak-portfolio-index-background-horizontal-alignment-center tweak-portfolio-index-background-delimiter-none tweak-portfolio-index-background-animation-type-fade tweak-portfolio-index-background-animation-duration-medium tweak-portfolio-hover-follow-layout-inline  tweak-portfolio-hover-follow-delimiter-bullet tweak-portfolio-hover-follow-animation-type-fade tweak-portfolio-hover-follow-animation-duration-fast tweak-portfolio-hover-static-layout-inline tweak-portfolio-hover-static-front tweak-portfolio-hover-static-delimiter-hyphen tweak-portfolio-hover-static-animation-type-fade tweak-portfolio-hover-static-animation-duration-fast tweak-product-basic-item-width-full tweak-product-basic-item-gallery-aspect-ratio-34-three-four-vertical tweak-product-basic-item-text-alignment-left tweak-product-basic-item-navigation-breadcrumbs tweak-product-basic-item-content-alignment-top tweak-product-basic-item-gallery-design-slideshow tweak-product-basic-item-gallery-placement-left tweak-product-basic-item-thumbnail-placement-side tweak-product-basic-item-click-action-none tweak-product-basic-item-hover-action-none tweak-product-basic-item-variant-picker-layout-dropdowns tweak-products-width-inset tweak-products-image-aspect-ratio-11-square tweak-products-text-alignment-left tweak-products-price-show tweak-products-nested-category-type-top tweak-products-category-title tweak-products-header-text-alignment-middle tweak-products-breadcrumbs primary-button-style-solid primary-button-shape-rounded secondary-button-style-solid secondary-button-shape-rounded tertiary-button-style-solid tertiary-button-shape-rounded image-block-poster-text-alignment-center image-block-card-content-position-center image-block-card-text-alignment-center image-block-overlap-content-position-center image-block-overlap-text-alignment-opposite image-block-collage-content-position-top image-block-collage-text-alignment-left image-block-stack-text-alignment-left hide-opentable-icons opentable-style-dark tweak-product-quick-view-button-style-floating tweak-product-quick-view-button-position-bottom tweak-product-quick-view-lightbox-excerpt-display-truncate tweak-product-quick-view-lightbox-show-arrows tweak-product-quick-view-lightbox-show-close-button tweak-product-quick-view-lightbox-controls-weight-light native-currency-code-usd collection-6298ea29a2382b218be83236 collection-type-page collection-layout-default homepage mobile-style-available sqs-seven-one
      
        
          
            
              
            
          
        
      
    " tabindex="-1">
  <div id="siteWrapper" class="clearfix site-wrapper">














    <header data-test="header" id="header" class="
    
      
    
    header theme-col--primary
  " data-controller="Header" data-current-styles="{
&quot;layout&quot;: &quot;navRight&quot;,
&quot;action&quot;: {
&quot;href&quot;: &quot;https://crosspointwaverly.onlinegiving.org/donate/login&quot;,
&quot;buttonText&quot;: &quot;Give&quot;,
&quot;newWindow&quot;: true
},
&quot;showSocial&quot;: false,
&quot;socialOptions&quot;: {
&quot;socialBorderShape&quot;: &quot;none&quot;,
&quot;socialBorderStyle&quot;: &quot;outline&quot;,
&quot;socialBorderThickness&quot;: {
&quot;unit&quot;: &quot;px&quot;,
&quot;value&quot;: 1.0
}
},
&quot;menuOverlayAnimation&quot;: &quot;fade&quot;,
&quot;cartStyle&quot;: &quot;cart&quot;,
&quot;cartText&quot;: &quot;Cart&quot;,
&quot;showEmptyCartState&quot;: true,
&quot;cartOptions&quot;: {
&quot;iconType&quot;: &quot;stroke-7&quot;,
&quot;cartBorderShape&quot;: &quot;none&quot;,
&quot;cartBorderStyle&quot;: &quot;outline&quot;,
&quot;cartBorderThickness&quot;: {
&quot;unit&quot;: &quot;px&quot;,
&quot;value&quot;: 1.0
}
},
&quot;showButton&quot;: true,
&quot;showCart&quot;: true,
&quot;showAccountLogin&quot;: true,
&quot;headerStyle&quot;: &quot;solid&quot;,
&quot;languagePicker&quot;: {
&quot;enabled&quot;: false,
&quot;iconEnabled&quot;: false,
&quot;iconType&quot;: &quot;globe&quot;,
&quot;flagShape&quot;: &quot;shiny&quot;,
&quot;languageFlags&quot;: [ ]
},
&quot;mobileOptions&quot;: {
&quot;layout&quot;: &quot;logoLeftNavRight&quot;,
&quot;menuIcon&quot;: &quot;doubleLineHamburger&quot;,
&quot;menuIconOptions&quot;: {
&quot;style&quot;: &quot;doubleLineHamburger&quot;,
&quot;thickness&quot;: {
&quot;unit&quot;: &quot;px&quot;,
&quot;value&quot;: 1.0
}
}
},
&quot;dynamicOptions&quot;: {
&quot;border&quot;: {
&quot;enabled&quot;: false,
&quot;position&quot;: &quot;allSides&quot;,
&quot;thickness&quot;: {
&quot;unit&quot;: &quot;px&quot;,
&quot;value&quot;: 4.0
}
}
},
&quot;solidOptions&quot;: {
&quot;headerOpacity&quot;: {
&quot;unit&quot;: &quot;%&quot;,
&quot;value&quot;: 100.0
},
&quot;border&quot;: {
&quot;enabled&quot;: false,
&quot;position&quot;: &quot;allSides&quot;,
&quot;thickness&quot;: {
&quot;unit&quot;: &quot;px&quot;,
&quot;value&quot;: 4.0
}
},
&quot;dropShadow&quot;: {
&quot;enabled&quot;: false,
&quot;blur&quot;: {
&quot;unit&quot;: &quot;px&quot;,
&quot;value&quot;: 30.0
},
&quot;spread&quot;: {
&quot;unit&quot;: &quot;px&quot;,
&quot;value&quot;: 0.0
},
&quot;distance&quot;: {
&quot;unit&quot;: &quot;px&quot;,
&quot;value&quot;: 0.0
}
},
&quot;blurBackground&quot;: {
&quot;enabled&quot;: false,
&quot;blurRadius&quot;: {
&quot;unit&quot;: &quot;px&quot;,
&quot;value&quot;: 12.0
}
}
},
&quot;gradientOptions&quot;: {
&quot;gradientType&quot;: &quot;faded&quot;,
&quot;headerOpacity&quot;: {
&quot;unit&quot;: &quot;%&quot;,
&quot;value&quot;: 90.0
},
&quot;border&quot;: {
&quot;enabled&quot;: false,
&quot;position&quot;: &quot;allSides&quot;,
&quot;thickness&quot;: {
&quot;unit&quot;: &quot;px&quot;,
&quot;value&quot;: 4.0
}
},
&quot;dropShadow&quot;: {
&quot;enabled&quot;: false,
&quot;blur&quot;: {
&quot;unit&quot;: &quot;px&quot;,
&quot;value&quot;: 30.0
},
&quot;spread&quot;: {
&quot;unit&quot;: &quot;px&quot;,
&quot;value&quot;: 0.0
},
&quot;distance&quot;: {
&quot;unit&quot;: &quot;px&quot;,
&quot;value&quot;: 0.0
}
},
&quot;blurBackground&quot;: {
&quot;enabled&quot;: false,
&quot;blurRadius&quot;: {
&quot;unit&quot;: &quot;px&quot;,
&quot;value&quot;: 12.0
}
}
},
&quot;showPromotedElement&quot;: false
}" data-section-id="header" data-header-theme="" data-menu-overlay-theme="" data-header-style="solid"
      data-language-picker="{
&quot;enabled&quot;: false,
&quot;iconEnabled&quot;: false,
&quot;iconType&quot;: &quot;globe&quot;,
&quot;flagShape&quot;: &quot;shiny&quot;,
&quot;languageFlags&quot;: [ ]
}" data-first-focusable-element tabindex="-1">

      <div class="sqs-announcement-bar-dropzone"></div>

      <div class="header-announcement-bar-wrapper">

        <a href="#page" tabindex="1" class="header-skip-link sqs-button-element--primary">
          Skip to Content
        </a>



        <style>
          @supports (-webkit-backdrop-filter: none) or (backdrop-filter: none) {
            .header-blur-background {

              -webkit-backdrop-filter: blur(12px);
              backdrop-filter: blur(12px);


            }
          }
        </style>
        <div class="header-border" data-header-style="solid" data-test="header-border" style="


  
    border-width: 0px !important;
  







"></div>
        <div class="header-dropshadow" data-header-style="solid" data-test="header-dropshadow" style="
  
    box-shadow: none;
  


"></div>


        <div>
          <div class="header-background-solid" data-header-style="solid" data-test="header-background-solid"
            style="opacity: calc(100 * .01)"></div>
        </div>


        <div class='header-inner container--fluid
      
        header-layout--with-commerce
      
      
      
       header-mobile-layout-logo-left-nav-right
      
      
      
      
      
      
       header-layout-nav-right
      
      
      
      
      
      
      
      
      ' style="


  
    padding: 0;
  







" data-test="header-inner">
          <!-- Background -->
          <div class="header-background theme-bg--primary"></div>

          <div class="header-display-desktop" data-content-field="site-title">















            <style>
              .top-bun,
              .patty,
              .bottom-bun {
                height: 1px;
              }
            </style>

            <!-- Burger -->
            <div class="header-burger

  menu-overlay-has-visible-non-navigation-items


  
" data-animation-role="header-element">
              <button class="header-burger-btn burger" data-test="header-burger">
                <span hidden class="js-header-burger-open-title visually-hidden">Open Menu</span>
                <span hidden class="js-header-burger-close-title visually-hidden">Close Menu</span>
                <div class="burger-box">
                  <div class="burger-inner header-menu-icon-doubleLineHamburger">
                    <div class="top-bun"></div>
                    <div class="patty"></div>
                    <div class="bottom-bun"></div>
                  </div>
                </div>
              </button>
            </div>


            <!-- Social -->



            <!-- Title and nav wrapper -->
            <div class="header-title-nav-wrapper">






              <!-- Title -->

              <div class="
                    header-title
                    
                  " data-animation-role="header-element">

                <div class="header-title-logo">
                  <a href="index.html" data-animation-role="header-element">











                    <img
                      src="{{ URL::asset('imageschurch/content/v1/6298e9ea65c3d6676b87e494/bb7d914c-e9f3-4bbc-a70e-f7ba89580b9b/cpc%2blogo26f7.jpg?format=1500w')}}" 
                      alt="Crosspoint Church">


                  </a>
                </div>



              </div>



              <!-- Nav -->
              <div class="header-nav">
                <div class="header-nav-wrapper">
                  <nav class="header-nav-list">




                    <div class="header-nav-item header-nav-item--folder">
                      <a class="header-nav-folder-title" href="team.html" tabindex="-1"
                        data-animation-role="header-element">
                        About
                      </a>
                      <div class="header-nav-folder-content">


                        <div class="header-nav-folder-item">
                          <a href="team.html">
                            <span class="header-nav-folder-item-content">
                              Our Team
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="beliefs.html">
                            <span class="header-nav-folder-item-content">
                              Our Beliefs
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="media.html">
                            <span class="header-nav-folder-item-content">
                              Our Media
                            </span>
                          </a>
                        </div>



                      </div>
                    </div>





                    <div class="header-nav-item header-nav-item--folder">
                      <a class="header-nav-folder-title" href="students.html" tabindex="-1"
                        data-animation-role="header-element">
                        Get Connected
                      </a>
                      <div class="header-nav-folder-content">


                        <div class="header-nav-folder-item">
                          <a href="students.html">
                            <span class="header-nav-folder-item-content">
                              Students
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="smallgroups.html">
                            <span class="header-nav-folder-item-content">
                              Small Groups
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="menandwomen.html">
                            <span class="header-nav-folder-item-content">
                              Men + Women
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="baptism.html">
                            <span class="header-nav-folder-item-content">
                              Baptism
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="worship.html">
                            <span class="header-nav-folder-item-content">
                              Worship
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="griefshare.html">
                            <span class="header-nav-folder-item-content">
                              Grief Share
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="growthtrack.html">
                            <span class="header-nav-folder-item-content">
                              Growth Track
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="soa.html">
                            <span class="header-nav-folder-item-content">
                              School of Arts
                            </span>
                          </a>
                        </div>



                      </div>
                    </div>





                    <div class="header-nav-item header-nav-item--collection">
                      <a href="events.html" data-animation-role="header-element">
                        Events
                      </a>
                    </div>






                    <div class="header-nav-item header-nav-item--folder">
                      <a class="header-nav-folder-title" href="kingdombuilders.html" tabindex="-1"
                        data-animation-role="header-element">
                        Kingdom Builders
                      </a>
                      <div class="header-nav-folder-content">


                        <div class="header-nav-folder-item">
                          <a href="kingdombuilders.html">
                            <span class="header-nav-folder-item-content">
                              Kingdom Builders
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="trips.html">
                            <span class="header-nav-folder-item-content">
                              Trips
                            </span>
                          </a>
                        </div>



                      </div>
                    </div>





                  </nav>
                </div>
              </div>


            </div>


            <!-- Actions -->
            <div class="header-actions header-actions--right">




















              <div class="showOnMobile">






                <div class="header-actions-action header-actions-action--cart">
                  <a href="cart.html"
                    class="cart-style-icon icon--stroke icon--fill icon--cart sqs-custom-cart  header-icon  show-empty-cart-state cart-quantity-zero header-icon-border-shape-none header-icon-border-style-outline">
                    <span class="Cart-inner">




                      <svg class="icon icon--cart" width="100" height="105" viewBox="0 0 100 105">
                        <path
                          d="M65.7324 22.5C65.7324 22.667 65.7291 22.8337 65.7226 23H73.7266C73.7305 22.8338 73.7324 22.6671 73.7324 22.5C73.7324 10.0736 62.9873 0 49.7324 0C36.4776 0 25.7324 10.0736 25.7324 22.5C25.7324 22.6671 25.7344 22.8338 25.7382 23H33.7422C33.7357 22.8337 33.7324 22.667 33.7324 22.5C33.7324 14.9704 40.4017 8 49.7324 8C59.0632 8 65.7324 14.9704 65.7324 22.5Z" />
                        <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M0.00857816 32.2592C-0.14118 29.9529 1.68905 28 4.00017 28H95.4638C97.775 28 99.6052 29.9529 99.4554 32.2592L94.9749 101.259C94.8383 103.363 93.0918 105 90.9833 105H8.48069C6.37218 105 4.62573 103.363 4.4891 101.259L0.00857816 32.2592ZM11.0475 94.1908L8.26834 36H91.1957L88.4166 94.1908C88.3147 96.3233 86.5561 98 84.4211 98H15.0429C12.9079 98 11.1493 96.3233 11.0475 94.1908Z" />
                      </svg>


                      <div class="icon-cart-quantity">

                        <span class="cart-quantity-container">

                          <span class="sqs-cart-quantity">0</span>

                        </span>

                      </div>
                    </span>
                  </a>
                </div>






              </div>


              <div class="showOnDesktop">






                <div class="header-actions-action header-actions-action--cart">
                  <a href="cart.html"
                    class="cart-style-icon icon--stroke icon--fill icon--cart sqs-custom-cart  header-icon  show-empty-cart-state cart-quantity-zero header-icon-border-shape-none header-icon-border-style-outline">
                    <span class="Cart-inner">




                      <svg class="icon icon--cart" width="100" height="105" viewBox="0 0 100 105">
                        <path
                          d="M65.7324 22.5C65.7324 22.667 65.7291 22.8337 65.7226 23H73.7266C73.7305 22.8338 73.7324 22.6671 73.7324 22.5C73.7324 10.0736 62.9873 0 49.7324 0C36.4776 0 25.7324 10.0736 25.7324 22.5C25.7324 22.6671 25.7344 22.8338 25.7382 23H33.7422C33.7357 22.8337 33.7324 22.667 33.7324 22.5C33.7324 14.9704 40.4017 8 49.7324 8C59.0632 8 65.7324 14.9704 65.7324 22.5Z" />
                        <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M0.00857816 32.2592C-0.14118 29.9529 1.68905 28 4.00017 28H95.4638C97.775 28 99.6052 29.9529 99.4554 32.2592L94.9749 101.259C94.8383 103.363 93.0918 105 90.9833 105H8.48069C6.37218 105 4.62573 103.363 4.4891 101.259L0.00857816 32.2592ZM11.0475 94.1908L8.26834 36H91.1957L88.4166 94.1908C88.3147 96.3233 86.5561 98 84.4211 98H15.0429C12.9079 98 11.1493 96.3233 11.0475 94.1908Z" />
                      </svg>


                      <div class="icon-cart-quantity">

                        <span class="cart-quantity-container">

                          <span class="sqs-cart-quantity">0</span>

                        </span>

                      </div>
                    </span>
                  </a>
                </div>






              </div>


              <div class="header-actions-action header-actions-action--cta" data-animation-role="header-element">
                <a class="btn btn--border theme-btn--primary-inverse sqs-button-element--primary"
                  href="https://crosspointwaverly.onlinegiving.org/donate/login" target="_blank">
                  Give
                </a>
              </div>

            </div>







          </div>
          <div class="header-display-mobile" data-content-field="site-title">


            <!-- Social -->



            <!-- Title and nav wrapper -->
            <div class="header-title-nav-wrapper">






              <!-- Title -->

              <div class="
                    header-title
                    
                  " data-animation-role="header-element">

                <div class="header-title-logo">
                  <a href="index.html" data-animation-role="header-element">











                    <img
                      src="{{ URL::asset('imageschurch/content/v1/6298e9ea65c3d6676b87e494/bb7d914c-e9f3-4bbc-a70e-f7ba89580b9b/cpc%2blogo26f7.jpg?format=1500w')}}" 
                      alt="Crosspoint Church">


                  </a>
                </div>



              </div>



              <!-- Nav -->
              <div class="header-nav">
                <div class="header-nav-wrapper">
                  <nav class="header-nav-list">




                    <div class="header-nav-item header-nav-item--folder">
                      <a class="header-nav-folder-title" href="team.html" tabindex="-1"
                        data-animation-role="header-element">
                        About
                      </a>
                      <div class="header-nav-folder-content">


                        <div class="header-nav-folder-item">
                          <a href="team.html">
                            <span class="header-nav-folder-item-content">
                              Our Team
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="beliefs.html">
                            <span class="header-nav-folder-item-content">
                              Our Beliefs
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="media.html">
                            <span class="header-nav-folder-item-content">
                              Our Media
                            </span>
                          </a>
                        </div>



                      </div>
                    </div>





                    <div class="header-nav-item header-nav-item--folder">
                      <a class="header-nav-folder-title" href="students.html" tabindex="-1"
                        data-animation-role="header-element">
                        Get Connected
                      </a>
                      <div class="header-nav-folder-content">


                        <div class="header-nav-folder-item">
                          <a href="students.html">
                            <span class="header-nav-folder-item-content">
                              Students
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="smallgroups.html">
                            <span class="header-nav-folder-item-content">
                              Small Groups
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="menandwomen.html">
                            <span class="header-nav-folder-item-content">
                              Men + Women
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="baptism.html">
                            <span class="header-nav-folder-item-content">
                              Baptism
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="worship.html">
                            <span class="header-nav-folder-item-content">
                              Worship
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="griefshare.html">
                            <span class="header-nav-folder-item-content">
                              Grief Share
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="growthtrack.html">
                            <span class="header-nav-folder-item-content">
                              Growth Track
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="soa.html">
                            <span class="header-nav-folder-item-content">
                              School of Arts
                            </span>
                          </a>
                        </div>



                      </div>
                    </div>





                    <div class="header-nav-item header-nav-item--collection">
                      <a href="events.html" data-animation-role="header-element">
                        Events
                      </a>
                    </div>
                    <div class="header-nav-item header-nav-item--folder">
                      <a class="header-nav-folder-title" href="kingdombuilders.html" tabindex="-1"
                        data-animation-role="header-element">
                        Kingdom Builders
                      </a>
                      <div class="header-nav-folder-content">


                        <div class="header-nav-folder-item">
                          <a href="kingdombuilders.html">
                            <span class="header-nav-folder-item-content">
                              Kingdom Builders
                            </span>
                          </a>
                        </div>




                        <div class="header-nav-folder-item">
                          <a href="trips.html">
                            <span class="header-nav-folder-item-content">
                              Trips
                            </span>
                          </a>
                        </div>



                      </div>
                    </div>





                  </nav>
                </div>
              </div>


            </div>


            <!-- Actions -->
            <div class="header-actions header-actions--right">




















              <div class="showOnMobile">






                <div class="header-actions-action header-actions-action--cart">
                  <a href="cart.html"
                    class="cart-style-icon icon--stroke icon--fill icon--cart sqs-custom-cart  header-icon  show-empty-cart-state cart-quantity-zero header-icon-border-shape-none header-icon-border-style-outline">
                    <span class="Cart-inner">




                      <svg class="icon icon--cart" width="100" height="105" viewBox="0 0 100 105">
                        <path
                          d="M65.7324 22.5C65.7324 22.667 65.7291 22.8337 65.7226 23H73.7266C73.7305 22.8338 73.7324 22.6671 73.7324 22.5C73.7324 10.0736 62.9873 0 49.7324 0C36.4776 0 25.7324 10.0736 25.7324 22.5C25.7324 22.6671 25.7344 22.8338 25.7382 23H33.7422C33.7357 22.8337 33.7324 22.667 33.7324 22.5C33.7324 14.9704 40.4017 8 49.7324 8C59.0632 8 65.7324 14.9704 65.7324 22.5Z" />
                        <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M0.00857816 32.2592C-0.14118 29.9529 1.68905 28 4.00017 28H95.4638C97.775 28 99.6052 29.9529 99.4554 32.2592L94.9749 101.259C94.8383 103.363 93.0918 105 90.9833 105H8.48069C6.37218 105 4.62573 103.363 4.4891 101.259L0.00857816 32.2592ZM11.0475 94.1908L8.26834 36H91.1957L88.4166 94.1908C88.3147 96.3233 86.5561 98 84.4211 98H15.0429C12.9079 98 11.1493 96.3233 11.0475 94.1908Z" />
                      </svg>


                      <div class="icon-cart-quantity">

                        <span class="cart-quantity-container">

                          <span class="sqs-cart-quantity">0</span>

                        </span>

                      </div>
                    </span>
                  </a>
                </div>






              </div>


              <div class="showOnDesktop">






                <div class="header-actions-action header-actions-action--cart">
                  <a href="cart.html"
                    class="cart-style-icon icon--stroke icon--fill icon--cart sqs-custom-cart  header-icon  show-empty-cart-state cart-quantity-zero header-icon-border-shape-none header-icon-border-style-outline">
                    <span class="Cart-inner">




                      <svg class="icon icon--cart" width="100" height="105" viewBox="0 0 100 105">
                        <path
                          d="M65.7324 22.5C65.7324 22.667 65.7291 22.8337 65.7226 23H73.7266C73.7305 22.8338 73.7324 22.6671 73.7324 22.5C73.7324 10.0736 62.9873 0 49.7324 0C36.4776 0 25.7324 10.0736 25.7324 22.5C25.7324 22.6671 25.7344 22.8338 25.7382 23H33.7422C33.7357 22.8337 33.7324 22.667 33.7324 22.5C33.7324 14.9704 40.4017 8 49.7324 8C59.0632 8 65.7324 14.9704 65.7324 22.5Z" />
                        <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M0.00857816 32.2592C-0.14118 29.9529 1.68905 28 4.00017 28H95.4638C97.775 28 99.6052 29.9529 99.4554 32.2592L94.9749 101.259C94.8383 103.363 93.0918 105 90.9833 105H8.48069C6.37218 105 4.62573 103.363 4.4891 101.259L0.00857816 32.2592ZM11.0475 94.1908L8.26834 36H91.1957L88.4166 94.1908C88.3147 96.3233 86.5561 98 84.4211 98H15.0429C12.9079 98 11.1493 96.3233 11.0475 94.1908Z" />
                      </svg>


                      <div class="icon-cart-quantity">

                        <span class="cart-quantity-container">

                          <span class="sqs-cart-quantity">0</span>

                        </span>

                      </div>
                    </span>
                  </a>
                </div>






              </div>


              <div class="header-actions-action header-actions-action--cta" data-animation-role="header-element">
                <a class="btn btn--border theme-btn--primary-inverse sqs-button-element--primary"
                  href="https://crosspointwaverly.onlinegiving.org/donate/login" target="_blank">
                  Give
                </a>
              </div>

            </div>




            <style>
              .top-bun,
              .patty,
              .bottom-bun {
                height: 1px;
              }
            </style>

            <!-- Burger -->
            <div class="header-burger

  menu-overlay-has-visible-non-navigation-items


  
" data-animation-role="header-element">
              <button class="header-burger-btn burger" data-test="header-burger">
                <span hidden class="js-header-burger-open-title visually-hidden">Open Menu</span>
                <span hidden class="js-header-burger-close-title visually-hidden">Close Menu</span>
                <div class="burger-box">
                  <div class="burger-inner header-menu-icon-doubleLineHamburger">
                    <div class="top-bun"></div>
                    <div class="patty"></div>
                    <div class="bottom-bun"></div>
                  </div>
                </div>
              </button>
            </div>


          </div>
        </div>
      </div>
      <!-- (Mobile) Menu Navigation -->



      @yield("content")


      <script type="text/javascript">
      const firstSection = document.querySelector('.page-section');
      const header = document.querySelector('.header');
      const mobileOverlayNav = document.querySelector('.header-menu');
      const sectionBackground = firstSection ? firstSection.querySelector('.section-background') : null;
      const headerHeight = header ? header.getBoundingClientRect().height : 0;
      const firstSectionHasBackground = firstSection ? firstSection.className.indexOf('has-background') >= 0 : false;
      const isFirstSectionInset = firstSection ? firstSection.className.indexOf('background-width--inset') >= 0 : false;
      const isLayoutEngineSection = firstSection ? firstSection.className.indexOf('layout-engine-section') >= 0 : false;

      if (firstSection) {
        firstSection.style.paddingTop = headerHeight + 'px';
      }
      if (sectionBackground && isLayoutEngineSection) {
        if (isFirstSectionInset) {
          sectionBackground.style.top = headerHeight + 'px';
        } else {
          sectionBackground.style.top = '';
        }
      }
        //# sourceURL=headerPositioning.js
    </script>


    <footer class="sections" id="footer-sections" data-footer-sections>



      <section data-test="page-section" data-section-theme="black-bold" class='page-section
    
      layout-engine-section
    
    background-width--full-bleed
    
      section-height--medium
    
    
      content-width--wide
    
    horizontal-alignment--center
    vertical-alignment--middle
    
      
    
    
    black-bold' data-section-id="6298e9ee65c3d6676b87e4f8" data-controller="SectionWrapperController"
        data-current-styles="{
&quot;imageOverlayOpacity&quot;: 0.15,
&quot;backgroundWidth&quot;: &quot;background-width--full-bleed&quot;,
&quot;sectionHeight&quot;: &quot;section-height--medium&quot;,
&quot;customSectionHeight&quot;: 20,
&quot;horizontalAlignment&quot;: &quot;horizontal-alignment--center&quot;,
&quot;verticalAlignment&quot;: &quot;vertical-alignment--middle&quot;,
&quot;contentWidth&quot;: &quot;content-width--wide&quot;,
&quot;customContentWidth&quot;: 100,
&quot;sectionTheme&quot;: &quot;black-bold&quot;,
&quot;sectionAnimation&quot;: &quot;none&quot;,
&quot;backgroundMode&quot;: &quot;image&quot;
}" data-current-context="{
&quot;video&quot;: {
&quot;playbackSpeed&quot;: 0.5,
&quot;filter&quot;: 1,
&quot;filterStrength&quot;: 0,
&quot;zoom&quot;: 0,
&quot;videoSourceProvider&quot;: &quot;none&quot;
},
&quot;backgroundImageId&quot;: null,
&quot;backgroundMediaEffect&quot;: null,
&quot;typeName&quot;: &quot;page&quot;
}" data-animation="none">
        <div class="section-border">
          <div class="section-background">



          </div>
        </div>
        <div class="content-wrapper" style='
      
        
      
    '>
        <div class="content">
          <div class="sqs-layout sqs-grid-12 columns-12" data-type="page-section"
              id="page-section-6298e9ee65c3d6676b87e4f8">
              <div class="row sqs-row">
                <div class="col sqs-col-12 span-12">
                  <div class="sqs-block newsletter-block sqs-block-newsletter" data-block-type="51"
                    id="block-yui_3_17_2_1_1654810668406_9661">
                    <div class="sqs-block-content">




                      <div class="newsletter-form-wrapper
  
  newsletter-form-wrapper--layoutFloat
  newsletter-form-wrapper--alignCenter
  
  ">
                        <form class="newsletter-form" data-form-id="62a26882b3aaa66851057911" autocomplete="on"
                          method="POST" novalidate onsubmit="return (function (form) {
    Y.use('squarespace-form-submit', 'node', function usingFormSubmit(Y) {
      (new Y.Squarespace.FormSubmit(form)).submit({
        formId: '62a26882b3aaa66851057911',
        collectionId: '',
        objectName: 'page-section-6298e9ee65c3d6676b87e4f8'
      });
    });
    return false;
  })(this);">
                          <header class="newsletter-form-header">
                            <h2 class="newsletter-form-header-title">Subscribe</h2>
                            <div class="newsletter-form-header-description">
                              <p class="" style="white-space:pre-wrap;">Sign up with your email address to receive news
                                and updates on all things Crosspoint!</p>
                            </div>
                          </header>
                          <div class="newsletter-form-body">
                            <div class="newsletter-form-fields-wrapper form-fields" style="vertical-align: middle;">



                              <div id="email-yui_3_17_2_1_1654810668406_9666"
                                class="newsletter-form-field-wrapper form-item field email required"
                                style="vertical-align: bottom;">
                                <label class="newsletter-form-field-label title"
                                  for="email-yui_3_17_2_1_1654810668406_9666-field">Email Address</label>
                                <input id="email-yui_3_17_2_1_1654810668406_9666-field"
                                  class="newsletter-form-field-element field-element" name="email"
                                  x-autocompletetype="email" autocomplete="email" type="email" spellcheck="false"
                                  placeholder="Email Address" />
                              </div>




                            </div>
                            <div data-animation-role="button" class="newsletter-form-button-wrapper submit-wrapper"
                              style="vertical-align: middle;">
                              <button class="
            newsletter-form-button
            sqs-system-button
            sqs-editable-button-layout
            sqs-editable-button-style
            sqs-editable-button-shape
            sqs-button-element--primary
          " type="submit" value="Sign Up">
                                <span class="newsletter-form-spinner sqs-spin light large"></span>
                                <span class="newsletter-form-button-label">Sign Up</span>
                                <span class="newsletter-form-button-icon"></span>
                              </button>
                            </div>

                            <div class="model"></div>


                          </div>
                          <div class="newsletter-form-footnote">
                            <p>We respect your privacy.</p>
                          </div>
                          <div class="hidden form-submission-text">Thank you!</div>
                          <div class="hidden form-submission-html" data-submission-html=""></div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="sqs-block html-block sqs-block-html" data-block-type="2" id="block-6cb4b7922455fc4458a5">
                    <div class="sqs-block-content">

                      <p style="text-align:center;white-space:pre-wrap;" class="">215 3rd St. NW, Waverly, IA 50677 |
                        (319) 483-5116 | emily@crosspointwaverly.com</p>


                    </div>
                  </div>
                  <div class="sqs-block socialaccountlinks-v2-block sqs-block-socialaccountlinks-v2"
                    data-block-type="54" id="block-yui_3_17_2_1_1660574164502_10992">
                    <div class="sqs-block-content">
                      <div
                        class="sqs-svg-icon--outer social-icon-alignment-center social-icons-color- social-icons-size-small social-icons-style-regular ">
                        <style>
                          #block-yui_3_17_2_1_1660574164502_10992 .social-icons-style-border .sqs-svg-icon--wrapper {

                            box-shadow: 0 0 0 2px inset;

                            border: none;
                          }
                        </style>
                        <nav class="sqs-svg-icon--list">
                          <a href="https://www.facebook.com/cpcwaverly/" target="_blank"
                            class="sqs-svg-icon--wrapper facebook-unauth" aria-label="Facebook">
                            <div>
                              <svg class="sqs-svg-icon--social" viewBox="0 0 64 64">
                                <use class="sqs-use--icon" xlink:href="#facebook-unauth-icon"></use>
                                <use class="sqs-use--mask" xlink:href="#facebook-unauth-mask"></use>
                              </svg>
                            </div>
                          </a><a href="https://www.youtube.com/channel/UCpFsNYvlHPRlS_-96JdbBZQ" target="_blank"
                            class="sqs-svg-icon--wrapper youtube-unauth" aria-label="YouTube">
                            <div>
                              <svg class="sqs-svg-icon--social" viewBox="0 0 64 64">
                                <use class="sqs-use--icon" xlink:href="#youtube-unauth-icon"></use>
                                <use class="sqs-use--mask" xlink:href="#youtube-unauth-mask"></use>
                              </svg>
                            </div>
                          </a><a href="https://www.instagram.com/cpcwaverly/" target="_blank"
                            class="sqs-svg-icon--wrapper instagram-unauth" aria-label="Instagram">
                            <div>
                              <svg class="sqs-svg-icon--social" viewBox="0 0 64 64">
                                <use class="sqs-use--icon" xlink:href="#instagram-unauth-icon"></use>
                                <use class="sqs-use--mask" xlink:href="#instagram-unauth-mask"></use>
                              </svg>
                            </div>
                          </a><a href="https://open.spotify.com/show/47YHBzNf52F6IhX9wJQESi" target="_blank"
                            class="sqs-svg-icon--wrapper spotify-unauth" aria-label="Spotify">
                            <div>
                              <svg class="sqs-svg-icon--social" viewBox="0 0 64 64">
                                <use class="sqs-use--icon" xlink:href="#spotify-unauth-icon"></use>
                                <use class="sqs-use--mask" xlink:href="#spotify-unauth-mask"></use>
                              </svg>
                            </div>
                          </a>
                        </nav>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </section>

    </footer>

  </div>

  <script defer="defer"
    src="{{ URL::asset('static1church/static/vta/5c5a519771c10ba3470d8101/scripts/site-bundle.ed9b392e652b0cd36cd65fc11bb1c05e.js')}}"
    type="text/javascript"></script>
  <svg xmlns="http://www.w3.org/2000/svg" version="1.1" style="display:none" data-usage="social-icons-svg">
    <symbol id="facebook-unauth-icon" viewBox="0 0 64 64">
      <path
        d="M34.1,47V33.3h4.6l0.7-5.3h-5.3v-3.4c0-1.5,0.4-2.6,2.6-2.6l2.8,0v-4.8c-0.5-0.1-2.2-0.2-4.1-0.2 c-4.1,0-6.9,2.5-6.9,7V28H24v5.3h4.6V47H34.1z" />
    </symbol>
    <symbol id="facebook-unauth-mask" viewBox="0 0 64 64">
      <path
        d="M0,0v64h64V0H0z M39.6,22l-2.8,0c-2.2,0-2.6,1.1-2.6,2.6V28h5.3l-0.7,5.3h-4.6V47h-5.5V33.3H24V28h4.6V24 c0-4.6,2.8-7,6.9-7c2,0,3.6,0.1,4.1,0.2V22z" />
    </symbol>
    <symbol id="youtube-unauth-icon" viewBox="0 0 64 64">
      <path
        d="M46.7,26c0,0-0.3-2.1-1.2-3c-1.1-1.2-2.4-1.2-3-1.3C38.3,21.4,32,21.4,32,21.4h0 c0,0-6.3,0-10.5,0.3c-0.6,0.1-1.9,0.1-3,1.3c-0.9,0.9-1.2,3-1.2,3S17,28.4,17,30.9v2.3c0,2.4,0.3,4.9,0.3,4.9s0.3,2.1,1.2,3 c1.1,1.2,2.6,1.2,3.3,1.3c2.4,0.2,10.2,0.3,10.2,0.3s6.3,0,10.5-0.3c0.6-0.1,1.9-0.1,3-1.3c0.9-0.9,1.2-3,1.2-3s0.3-2.4,0.3-4.9 v-2.3C47,28.4,46.7,26,46.7,26z M28.9,35.9l0-8.4l8.1,4.2L28.9,35.9z" />
    </symbol>
    <symbol id="youtube-unauth-mask" viewBox="0 0 64 64">
      <path
        d="M0,0v64h64V0H0z M47,33.1c0,2.4-0.3,4.9-0.3,4.9s-0.3,2.1-1.2,3c-1.1,1.2-2.4,1.2-3,1.3 C38.3,42.5,32,42.6,32,42.6s-7.8-0.1-10.2-0.3c-0.7-0.1-2.2-0.1-3.3-1.3c-0.9-0.9-1.2-3-1.2-3S17,35.6,17,33.1v-2.3 c0-2.4,0.3-4.9,0.3-4.9s0.3-2.1,1.2-3c1.1-1.2,2.4-1.2,3-1.3c4.2-0.3,10.5-0.3,10.5-0.3h0c0,0,6.3,0,10.5,0.3c0.6,0.1,1.9,0.1,3,1.3 c0.9,0.9,1.2,3,1.2,3s0.3,2.4,0.3,4.9V33.1z M28.9,35.9l8.1-4.2l-8.1-4.2L28.9,35.9z" />
    </symbol>
    <symbol id="instagram-unauth-icon" viewBox="0 0 64 64">
      <path
        d="M46.91,25.816c-0.073-1.597-0.326-2.687-0.697-3.641c-0.383-0.986-0.896-1.823-1.73-2.657c-0.834-0.834-1.67-1.347-2.657-1.73c-0.954-0.371-2.045-0.624-3.641-0.697C36.585,17.017,36.074,17,32,17s-4.585,0.017-6.184,0.09c-1.597,0.073-2.687,0.326-3.641,0.697c-0.986,0.383-1.823,0.896-2.657,1.73c-0.834,0.834-1.347,1.67-1.73,2.657c-0.371,0.954-0.624,2.045-0.697,3.641C17.017,27.415,17,27.926,17,32c0,4.074,0.017,4.585,0.09,6.184c0.073,1.597,0.326,2.687,0.697,3.641c0.383,0.986,0.896,1.823,1.73,2.657c0.834,0.834,1.67,1.347,2.657,1.73c0.954,0.371,2.045,0.624,3.641,0.697C27.415,46.983,27.926,47,32,47s4.585-0.017,6.184-0.09c1.597-0.073,2.687-0.326,3.641-0.697c0.986-0.383,1.823-0.896,2.657-1.73c0.834-0.834,1.347-1.67,1.73-2.657c0.371-0.954,0.624-2.045,0.697-3.641C46.983,36.585,47,36.074,47,32S46.983,27.415,46.91,25.816z M44.21,38.061c-0.067,1.462-0.311,2.257-0.516,2.785c-0.272,0.7-0.597,1.2-1.122,1.725c-0.525,0.525-1.025,0.85-1.725,1.122c-0.529,0.205-1.323,0.45-2.785,0.516c-1.581,0.072-2.056,0.087-6.061,0.087s-4.48-0.015-6.061-0.087c-1.462-0.067-2.257-0.311-2.785-0.516c-0.7-0.272-1.2-0.597-1.725-1.122c-0.525-0.525-0.85-1.025-1.122-1.725c-0.205-0.529-0.45-1.323-0.516-2.785c-0.072-1.582-0.087-2.056-0.087-6.061s0.015-4.48,0.087-6.061c0.067-1.462,0.311-2.257,0.516-2.785c0.272-0.7,0.597-1.2,1.122-1.725c0.525-0.525,1.025-0.85,1.725-1.122c0.529-0.205,1.323-0.45,2.785-0.516c1.582-0.072,2.056-0.087,6.061-0.087s4.48,0.015,6.061,0.087c1.462,0.067,2.257,0.311,2.785,0.516c0.7,0.272,1.2,0.597,1.725,1.122c0.525,0.525,0.85,1.025,1.122,1.725c0.205,0.529,0.45,1.323,0.516,2.785c0.072,1.582,0.087,2.056,0.087,6.061S44.282,36.48,44.21,38.061z M32,24.297c-4.254,0-7.703,3.449-7.703,7.703c0,4.254,3.449,7.703,7.703,7.703c4.254,0,7.703-3.449,7.703-7.703C39.703,27.746,36.254,24.297,32,24.297z M32,37c-2.761,0-5-2.239-5-5c0-2.761,2.239-5,5-5s5,2.239,5,5C37,34.761,34.761,37,32,37z M40.007,22.193c-0.994,0-1.8,0.806-1.8,1.8c0,0.994,0.806,1.8,1.8,1.8c0.994,0,1.8-0.806,1.8-1.8C41.807,22.999,41.001,22.193,40.007,22.193z" />
    </symbol>
    <symbol id="instagram-unauth-mask" viewBox="0 0 64 64">
      <path
        d="M43.693,23.153c-0.272-0.7-0.597-1.2-1.122-1.725c-0.525-0.525-1.025-0.85-1.725-1.122c-0.529-0.205-1.323-0.45-2.785-0.517c-1.582-0.072-2.056-0.087-6.061-0.087s-4.48,0.015-6.061,0.087c-1.462,0.067-2.257,0.311-2.785,0.517c-0.7,0.272-1.2,0.597-1.725,1.122c-0.525,0.525-0.85,1.025-1.122,1.725c-0.205,0.529-0.45,1.323-0.516,2.785c-0.072,1.582-0.087,2.056-0.087,6.061s0.015,4.48,0.087,6.061c0.067,1.462,0.311,2.257,0.516,2.785c0.272,0.7,0.597,1.2,1.122,1.725s1.025,0.85,1.725,1.122c0.529,0.205,1.323,0.45,2.785,0.516c1.581,0.072,2.056,0.087,6.061,0.087s4.48-0.015,6.061-0.087c1.462-0.067,2.257-0.311,2.785-0.516c0.7-0.272,1.2-0.597,1.725-1.122s0.85-1.025,1.122-1.725c0.205-0.529,0.45-1.323,0.516-2.785c0.072-1.582,0.087-2.056,0.087-6.061s-0.015-4.48-0.087-6.061C44.143,24.476,43.899,23.682,43.693,23.153z M32,39.703c-4.254,0-7.703-3.449-7.703-7.703s3.449-7.703,7.703-7.703s7.703,3.449,7.703,7.703S36.254,39.703,32,39.703z M40.007,25.793c-0.994,0-1.8-0.806-1.8-1.8c0-0.994,0.806-1.8,1.8-1.8c0.994,0,1.8,0.806,1.8,1.8C41.807,24.987,41.001,25.793,40.007,25.793z M0,0v64h64V0H0z M46.91,38.184c-0.073,1.597-0.326,2.687-0.697,3.641c-0.383,0.986-0.896,1.823-1.73,2.657c-0.834,0.834-1.67,1.347-2.657,1.73c-0.954,0.371-2.044,0.624-3.641,0.697C36.585,46.983,36.074,47,32,47s-4.585-0.017-6.184-0.09c-1.597-0.073-2.687-0.326-3.641-0.697c-0.986-0.383-1.823-0.896-2.657-1.73c-0.834-0.834-1.347-1.67-1.73-2.657c-0.371-0.954-0.624-2.044-0.697-3.641C17.017,36.585,17,36.074,17,32c0-4.074,0.017-4.585,0.09-6.185c0.073-1.597,0.326-2.687,0.697-3.641c0.383-0.986,0.896-1.823,1.73-2.657c0.834-0.834,1.67-1.347,2.657-1.73c0.954-0.371,2.045-0.624,3.641-0.697C27.415,17.017,27.926,17,32,17s4.585,0.017,6.184,0.09c1.597,0.073,2.687,0.326,3.641,0.697c0.986,0.383,1.823,0.896,2.657,1.73c0.834,0.834,1.347,1.67,1.73,2.657c0.371,0.954,0.624,2.044,0.697,3.641C46.983,27.415,47,27.926,47,32C47,36.074,46.983,36.585,46.91,38.184z M32,27c-2.761,0-5,2.239-5,5s2.239,5,5,5s5-2.239,5-5S34.761,27,32,27z" />
    </symbol>
    <symbol id="spotify-unauth-icon" viewBox="0 0 64 64">
      <path
        d="M32,16c-8.8,0-16,7.2-16,16c0,8.8,7.2,16,16,16c8.8,0,16-7.2,16-16C48,23.2,40.8,16,32,16 M39.3,39.1c-0.3,0.5-0.9,0.6-1.4,0.3c-3.8-2.3-8.5-2.8-14.1-1.5c-0.5,0.1-1.1-0.2-1.2-0.7c-0.1-0.5,0.2-1.1,0.8-1.2 c6.1-1.4,11.3-0.8,15.5,1.8C39.5,38,39.6,38.6,39.3,39.1 M41.3,34.7c-0.4,0.6-1.1,0.8-1.7,0.4c-4.3-2.6-10.9-3.4-15.9-1.9 c-0.7,0.2-1.4-0.2-1.6-0.8c-0.2-0.7,0.2-1.4,0.8-1.6c5.8-1.8,13-0.9,18,2.1C41.5,33.4,41.7,34.1,41.3,34.7 M41.5,30.2 c-5.2-3.1-13.7-3.3-18.6-1.9c-0.8,0.2-1.6-0.2-1.9-1c-0.2-0.8,0.2-1.6,1-1.9c5.7-1.7,15-1.4,21,2.1c0.7,0.4,0.9,1.3,0.5,2.1 C43.1,30.4,42.2,30.6,41.5,30.2" />
    </symbol>
    <symbol id="spotify-unauth-mask" viewBox="0 0 64 64">
      <path
        d="M39,37.7c-4.2-2.6-9.4-3.2-15.5-1.8c-0.5,0.1-0.9,0.7-0.8,1.2c0.1,0.5,0.7,0.9,1.2,0.7c5.6-1.3,10.3-0.8,14.1,1.5 c0.5,0.3,1.1,0.1,1.4-0.3C39.6,38.6,39.5,38,39,37.7z M40.9,33c-4.9-3-12.2-3.9-18-2.1c-0.7,0.2-1,0.9-0.8,1.6 c0.2,0.7,0.9,1,1.6,0.8c5.1-1.5,11.6-0.8,15.9,1.9c0.6,0.4,1.4,0.2,1.7-0.4C41.7,34.1,41.5,33.4,40.9,33z M0,0v64h64V0H0z M32,48 c-8.8,0-16-7.2-16-16c0-8.8,7.2-16,16-16c8.8,0,16,7.2,16,16C48,40.8,40.8,48,32,48z M43,27.6c-5.9-3.5-15.3-3.9-21-2.1 c-0.8,0.2-1.2,1.1-1,1.9c0.2,0.8,1.1,1.2,1.9,1c4.9-1.5,13.4-1.2,18.6,1.9c0.7,0.4,1.6,0.2,2.1-0.5C43.9,29,43.7,28,43,27.6z" />
    </symbol>
  </svg>

</body>


</html>