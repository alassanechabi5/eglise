<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HistoryGallery;
use App\History;
class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
       $histoires = History::all();
    
        return view('histories.index',compact('histoires'));
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('histories.create');//

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'bail|required|string|max:255',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            
        ]);
     
    $hist = new History;
    $hist->title = $request->title;
    $hist->date_debut = $request->date_debut;
    $hist->date_fin = $request->date_fin;
    $hist->comment = $request->comment;

    $hist->save();
    if($request->hasfile('images'))
     {

        foreach($request->file('images') as $image)
        {
            $name=time().$image->getClientOriginalName();
            $image->move(public_path().'/histories/', $name);  

        $gallery =  new  HistoryGallery;
        $gallery->image = $name;
        $gallery->history_id = $hist->id;

        $gallery->save();

        }

        return back()->with('success', 'Your images has been successfully');

     }

      
  

    return back()->with('error', 'No images');
    }

    
    public function uploadImg(Request $request){
       if ($request->hasFile('images'))
        {
            foreach($request->file('images') as $image)
        {
            $input['imagename'] = ($image->getClientOriginalExtension() != '') ? time().'.'.$image->getClientOriginalExtension() : time().'.jpg';
   
            $destinationPath = public_path('/histories');
  
            $image->move($destinationPath, $input['imagename']);
        }
           
            return response()->json(['status' => true,]);
        }
        return response()->json(['status' => false, 'text' => "No photo file"]);
      }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
